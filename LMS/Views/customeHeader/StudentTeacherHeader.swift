//
//  StudentTeacherHeader.swift
//  LMS
//
//  Created by Ishika Gupta on 05/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class StudentTeacherHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var viewSeprator: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var addUserButton: UIButton!

}
