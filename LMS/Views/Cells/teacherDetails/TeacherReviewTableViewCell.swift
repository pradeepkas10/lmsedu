//
//  TeacherReviewTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class TeacherReviewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: UIView!

    var arrReview = [[String:AnyObject]]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "ReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "ReviewTableViewCell")

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataSource(dataSource:[String:AnyObject]){
        print("review ===\(dataSource["reviews"] )")
        
        if let reviews = dataSource["reviews"] as? [[String:AnyObject]]{
            headerView.isHidden = false
            arrReview = reviews
            tblView.reloadData()
        }

    }
    
    
}

extension TeacherReviewTableViewCell : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cellLinks  = tableView.dequeueReusableCell(withIdentifier: "ReviewTableViewCell", for: indexPath) as? ReviewTableViewCell else {
            return UITableViewCell()
        }
        cellLinks.setData(dict: arrReview[indexPath.item])
        cellLinks.selectionStyle = .none
        return cellLinks
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
}

