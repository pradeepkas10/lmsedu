//
//  TeacherDetailHeaderCell.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class TeacherDetailHeaderCell: UITableViewCell {

    
    @IBOutlet weak var btnMaker: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var isStudent = false


    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dict:[String:AnyObject],inviteMarker:Bool){
        
        var name = ""
        
        if inviteMarker{
            btnMaker.isEnabled = false
            btnMaker.setTitle("MARKER ALREADY INVITED", for: .normal)
        }else{
            btnMaker.isEnabled = true
            btnMaker.setTitle("INVITE MARKER", for: .normal)
        }
        
        
        if isStudent {
            btnMaker.isEnabled = true
            btnMaker.setTitle("EDIT", for: .normal)

        }

        
        if let FName = dict["first_name"] as? String{
            name = FName
        }
        if let LName = dict["last_name"] as? String{
            self.lblName.text = name + " " + LName
        }
        
        if let price = dict["rate"] as? String{
            self.lblPrice.text = "£" + price + " /hr"
        }
        
        if let title = dict["title"] as? String{
            self.lblTitle.text = title
        }
        
        if let city = dict["city"] as? [String:AnyObject]{
            self.lblLocation.text = "Location : \(city["name"] as? String ?? "")"
        }

        
        if let img = dict["avatar"] as? String {
            Alamofire.request(img).responseImage { response in
                debugPrint(response)
                if let image = response.result.value {
                    self.imgView.image = image
                    print("image downloaded: \(image)")
                }
            }
            
        }
        
        
        
        
    }

    
}
