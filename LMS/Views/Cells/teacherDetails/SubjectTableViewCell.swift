//
//  SubjectTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {

    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDesp: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func setData(dict:[String:AnyObject],index:IndexPath){
        switch index.item {
        case 1:
            lblHeading.text = "Subjects"
            if let arr = dict["subjects"] as? [[String:AnyObject]]{
                let names = arr.map({$0["name"] as? String ?? ""})
                lblDesp.text = names.joined(separator: " ,")
            }
        case 2:
            lblHeading.text = "Background"
            lblDesp.text = dict["skills"] as? String ?? ""
        case 3:
            lblHeading.text = "Qualification"
            if let arr = dict["qualifications"] as? [[String:AnyObject]]{
                let names = arr.map({$0["name"] as? String ?? ""})
                lblDesp.text = names.joined(separator: " ,")
            }

        default:
            print("")
        }
        
        
    }
    
    
}
