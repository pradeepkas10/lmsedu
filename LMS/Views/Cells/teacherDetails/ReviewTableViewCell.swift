//
//  ReviewTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import FloatRatingView

class ReviewTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var flotingView: FloatRatingView!

    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dict:[String:AnyObject]){
        
        let reviewBy = dict["review_by"] as? [String:AnyObject]
        var name = ""
        if let FName = reviewBy!["first_name"] as? String{
            name = FName
        }
        if let LName = reviewBy!["last_name"] as? String{
            self.lblName.text = name + " " + LName
        }

        if let rating = dict["rating"] as? String{
            flotingView.rating = Double(rating) ?? 0.0
            
        }
        
        if let review = dict["review"] as? String{
            lblReview.text = review
        }
        
        if let date = dict["created_date"] as? String{
            lblDate.text = self.getDateFormString(dS: date, getFormat: "h:mm dd-MMM-yyyy")
        }

        
        //2019-07-17T16:18:10.880546Z" 9:48 17th Jul 2019
        

    }
    
    
    func getDateFormString(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let d = dateFormate.date(from: dS)
        
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)
        
    }

    
    
    
}
