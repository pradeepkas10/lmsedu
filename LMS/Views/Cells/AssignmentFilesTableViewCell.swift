//
//  AssignmentFilesTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 10/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class AssignmentFilesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtView: UITextView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setData(dict:[String:AnyObject]){
        
        var str = ""
        if let files = dict["job_files"] as? [[String:AnyObject]]{
            for ele in files{
                str = str + ((ele["file"] as? String)!) + "\n"
            }
        }
        
        txtView.text = str
        
    }
    
}
