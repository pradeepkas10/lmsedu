//
//  ClassAssignmentTableViewCell.swift
//  LMS
//
//  Created by Ishika Gupta on 04/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ClassAssignmentTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(dict:[String:AnyObject]){
        
        if let name = dict["name"] as? String{
            self.lblName.text = name
        }
        
        
        if let dueDate = dict["due_date"] as? String{
            self.lblCreatedDate.text = "Due: " + getDateFormString(dS: dueDate, getFormat: "MMM dd,yyyy")
        }
    }
    
    func getDateFormString(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        var dateStr = dS
        if !dateStr.contains("."){
            dateStr = dateStr.replacingOccurrences(of: "Z", with: ".00Z")
        }
        let d = dateFormate.date(from: dateStr)
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)
        
    }
    
    func getDateFormStringCreated(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        let d = dateFormate.date(from: dS)
        
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)
        
    }
    
}
