//
//  MainCollectionCell.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class MainCollectionCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var cView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cView.layer.cornerRadius = 4.0
        cView.addShadowTo()
    }
    
    func setUpData(data:[String:Any]) {
        
        titleLabel.text = data["title"] as? String ?? ""
        imageView.image = UIImage(named:  data["image"] as? String ?? "")
    }

}
