//
//  TeachersTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 15/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class TeachersTableViewCell: UITableViewCell {

    @IBOutlet weak var btnHire: UIButton!
    @IBOutlet weak var lblAccuracy: UILabel!
    @IBOutlet weak var lblMarkingTime: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        //btnHire.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 18)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dict:[String:AnyObject]){
        
        //btnHire.isHidden = true
        var name = ""
        if let FName = dict["first_name"] as? String{
            name = FName
        }
        if let LName = dict["last_name"] as? String{
            self.lblName.text = name + " " + LName
        }
        
        if let price = dict["rate"] as? String{
            self.lblPrice.text = "£" + price + " /hr"
        }

        if let img = dict["avatar"] as? String{
            Alamofire.request(img).responseImage { response in
                debugPrint(response)
                if let image = response.result.value {
                    self.imgView.image = image
                    print("image downloaded: \(image)")
                }
            }

        }

        
        
        
    }
    
    
    
}
