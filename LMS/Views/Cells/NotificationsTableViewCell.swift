//
//  NotificationsTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 16/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var informationText: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setData(dict: [String:Any]) {
        let notif = dict["notification"] as? [String: Any]
        
        informationText.text = notif?["verb"] as? String ?? ""
        if let date = notif?["created"] as? String {
            dateLabel.text = getDateFormString(dS: date, getFormat: "h:mm dd-MMM-yyyy")
        }
        //date.text = notif?["created"] as? String ?? ""
    }
    
   func getDateFormString(dS:String,getFormat:String)->String{
       
    let dateFormate = DateFormatter()
       dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
       let d = dateFormate.date(from: dS)
       
       let dateF = DateFormatter()
       dateF.dateFormat = getFormat
       
       return dateF.string(from: d!)
   }
    
}
