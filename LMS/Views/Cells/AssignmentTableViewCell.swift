//
//  AssignmentTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 10/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class AssignmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var btnOptions: UIButton!
    @IBOutlet weak var lblCreatedDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(dict:[String:AnyObject]){
        
        if let name = dict["name"] as? String{
            self.lblName.text = name
        }
        
        if let CDate = dict["created_date"] as? String{
            self.lblCreatedDate.text = "Created " + getDateFormStringCreated(dS: CDate, getFormat: "MMM dd")
        }
        
        if let dueDate = dict["due"] as? String{
            self.lblDueDate.text = "Due " + getDateFormString(dS: dueDate, getFormat: "MMM dd,yyyy")
        }
    }
    
    func getDateFormString(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        var dateStr = dS
        if !dateStr.contains("."){
            dateStr = dateStr.replacingOccurrences(of: "Z", with: ".00Z")
        }
        let d = dateFormate.date(from: dateStr)
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)

    }
    
    func getDateFormStringCreated(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSZ"
        let d = dateFormate.date(from: dS)
        
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)
        
    }
}


//{"id":148,"name":"testing final","description":"testing testing testing","level":1,"due":"2019-07-08T00:00:00Z","rate":"3.00","created_date":"2019-07-05T11:58:50.902748Z","is_moderation":false,"display_rate":"£3.00"}
