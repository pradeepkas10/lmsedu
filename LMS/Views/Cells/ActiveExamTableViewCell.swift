//
//  ActiveExamTableViewCell.swift
//  LMS
//
//  Created by Ishika Gupta on 06/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ActiveExamTableViewCell: UITableViewCell {

        
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTeacherName: UILabel!
    @IBOutlet weak var lblExamName: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnStatus.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 18)
        // Initialization code
    }
    
    
    func setUpData(data:[String:Any]) {
        
    }
    
}
