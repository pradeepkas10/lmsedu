//
//  ClassesCollectionViewCell.swift
//  LMS
//
//  Created by Ishika Gupta on 04/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class ClassesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStudentNum: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(dict:[String:Any]){
        
//[{"id":23,"school":null,"name":"Test class","level":2,"subject":2,"students_count":1,"avatar":{"id":4,"image":"https://api.progressay.mynividata.in/media/avatar/maths.png","label":"Maths"}}]

       
        if let name = dict["name"] as? String {
            self.lblName.text = name
        }
        
        self.lblStudentNum.text = "\(String(describing: dict["students_count"] ?? "0")) Students"
        
        let dictAvt = dict["avatar"] as? [String: Any]
        
        if let img = dictAvt?["image"] as? String {
            Alamofire.request(img).responseImage { response in
                debugPrint(response)
                if let image = response.result.value {
                    self.imgView.image = image
                    print("image downloaded: \(image)")
                }
            }
            
        }
        
    }
    
}
