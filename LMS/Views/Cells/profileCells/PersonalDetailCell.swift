//
//  PersonalDetailCell.swift
//  LMS
//
//  Created by Ishika Gupta on 20/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class PersonalDetailCell: UITableViewCell {

    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var city: UITextField!
    @IBOutlet weak var pincode: UITextField!
    @IBOutlet weak var addressTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        firstName.setBottomBorder()
        lastName.setBottomBorder()
        city.setBottomBorder()
        pincode.setBottomBorder()
    }
    
    func setData(dict: [String: Any]) {
        
        firstName.text = dict["first_name"] as? String ?? ""
        lastName.text = dict["last_name"] as? String ?? ""
        
        let personal_address = dict["personal_address"] as? [String: Any]
        
        city.text = personal_address?["city"] as? String ?? ""
        pincode.text = personal_address?["postal_code"] as? String ?? ""
        
        let address = "\(personal_address?["line1"] ?? "") /n \(personal_address?["line2"] ?? "") \(personal_address?["city"] ?? "") \(personal_address?["country"] ?? "")"
        
        addressTextView.text = address
    }

}
