//
//  ProfileDetailTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 28/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ProfileDetailTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    @IBOutlet weak var arrowCenter: NSLayoutConstraint!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setData(index:IndexPath,dict:[String:AnyObject]){
        
        var name = ""
        if let Fname = dict["first_name"] as? String{
            name = Fname
        }
        if let Lname = dict["last_name"] as? String{
            name = name + " " + Lname
        }
        arrowCenter.constant = 0

        switch index.item {
        case 1:
            lblName.text = "NAME"
            lblValue.text = name
        case 2:
            lblName.text = "EMAIL"
            if let email = dict["email"] as? String{
                lblValue.text = email
            }
        case 3:
            lblName.text = "USERNAME"
            if let email = dict["email"] as? String{
                lblValue.text = email
            }
        case 4:
            lblName.text = "CHANGE PASSWORD"
            lblValue.text = ""
            arrowCenter.constant = -8
        default:
            print("default")
        }


    }
    
}
