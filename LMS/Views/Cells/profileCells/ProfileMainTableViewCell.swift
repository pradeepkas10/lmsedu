//
//  ProfileMainTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 28/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ProfileMainTableViewCell: UITableViewCell {

    @IBOutlet weak var btnTakeTest: UIButton!
    @IBOutlet weak var lblEssay: UILabel!
    @IBOutlet weak var lblMarks: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnTakeTest.isHidden = true
        btnTakeTest.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 18)
        // Initialization code
    }

    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
