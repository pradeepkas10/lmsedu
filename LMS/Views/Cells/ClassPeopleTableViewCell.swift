//
//  ClassPeopleTableViewCell.swift
//  LMS
//
//  Created by Ishika Gupta on 07/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ClassPeopleTableViewCell: UITableViewCell {

    @IBOutlet weak var namelbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
    func setUpData(data:[String:Any]) {
        print(data)
        let fName = data["first_name"] as? String ?? ""
        let lName = data["last_name"] as? String ?? ""
        
        namelbl.text =  "\(fName) \(lName)"
    }
}
