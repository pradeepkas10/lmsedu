//
//  DetailAssignmentTableViewCell.swift
//  LMS
//
//  Created by Wemonde on 10/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class DetailAssignmentTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var lblDueOn: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
  //  let foundItems = found.filter { $0.itemName == filterItemName }
    

    
    func setData(dict:[String:AnyObject]){
        if let name = dict["name"] as? String{
            lblName.text = name
        }
        
        if let desp = dict["description"] as? String{
            lblDescription.text = desp
        }
        
        if let dueDate = dict["due"] as? String{
            lblDueOn.text =  getDateFormString(dS: dueDate, getFormat: "MMM dd,yyyy")
        }
        
        if let data = AppHelper.getDictFromUserDefault(key: "IntialData") as? [String:AnyObject]{
            let arrLevel = data["levels"] as! [[String:AnyObject]]
            let arrSubject = data["subjects"] as! [[String:AnyObject]]
            
            if  let idLevel = dict["level"] as? Int {
                lblLevel.text = arrLevel.filter({($0["id"] as? Int) == idLevel}).first!["name"] as? String ?? ""
            }
            
            if  let idSubject = dict["subject"] as? Int {
                 lblSubject.text = arrSubject.filter({($0["id"] as? Int) == idSubject}).first?["name"] as? String ?? ""
            }

        }

        
    }
    
    
    func getDateFormString(dS:String,getFormat:String)->String{
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let d = dateFormate.date(from: dS)
        
        let dateF = DateFormatter()
        dateF.dateFormat = getFormat
        
        return dateF.string(from: d!)
        
    }
    
    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
