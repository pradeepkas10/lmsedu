//
//  ApplicationTableViewCell.swift
//  LMS
//
//  Created by Ishika Gupta on 06/09/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ApplicationTableViewCell: UITableViewCell {

    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var sujectlbl: UILabel!
    @IBOutlet weak var levellbl: UILabel!
    @IBOutlet weak var namelbl: UILabel!
    @IBOutlet weak var pricelbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnStatus.roundBorder()
        //btnStatus.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 18)
        // Initialization code
    }
    
    
    func setUpData(data:[String:Any]) {
        print(data)
        
        pricelbl.text = data["display_price"] as? String ?? ""
        
        let dict = data["job"] as? [String:Any]
        namelbl.text = dict?["name"] as? String ?? ""
        
        if data["status"] as? String ?? "" == "requested_by_student" {
            btnStatus.setTitle("Request sent", for: .normal)
        } else if data["status"] as? String ?? "" == "evaluation_completed" {
            btnStatus.setTitle("Marking comp", for: .normal)
        }
        
//        sujectlbl.text = "\(data["subject"] as? Int ?? 0)"
//        levellbl.text = "\(data["level"] as? Int ?? 0)"
        
        
        if let data = AppHelper.getDictFromUserDefault(key: "IntialData") as? [String:AnyObject]{
            let arrLevel = data["levels"] as! [[String:AnyObject]]
            let arrSubject = data["subjects"] as! [[String:AnyObject]]
            
            if  let idLevel = data["level"] as? Int {
                levellbl.text = arrLevel.filter({($0["id"] as? Int) == idLevel}).first!["name"] as? String ?? ""
            }
            
            if  let idSubject = data["subject"] as? Int {
                 sujectlbl.text = arrSubject.filter({($0["id"] as? Int) == idSubject}).first?["name"] as? String ?? ""
            }

        }
        
        
    }
}
