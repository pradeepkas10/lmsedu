//
//  BankDetailView.swift
//  LMS
//
//  Created by Ishika Gupta on 29/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class BankDetailView: UIView {
    
    @IBOutlet weak var accountHolderName: UITextField!
    @IBOutlet weak var accountNumber: UITextField!
    @IBOutlet weak var shortCode: UITextField!
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func btnCloseClicked(sender:UIButton){
    }
    
    @IBAction func btnCancelClicked(sender:UIButton){
    }
    
    @IBAction func btnAddBankAccount(sender:UIButton){
    }

}
