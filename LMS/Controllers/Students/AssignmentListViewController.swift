//
//  AssignmentListViewController.swift
//  LMS
//
//  Created by Wemonde on 10/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class AssignmentListViewController: UIViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrListing = [[String:AnyObject]]()
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "AssignmentTableViewCell", bundle: nil), forCellReuseIdentifier: "AssignmentTableViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getResponselist(limit: 0)
        
    }
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton) {
    }
    
    
    //MARK:- server
    func  getResponselist(limit:Int){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.MyJobs.rawValue + "?limit=100&offset=0")
        var header = [String:String]()
        //        market-place/my-jobs/?limit=10&offset=0
        
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String{
            header = ["Authorization" : "Token \(key)"]
        }
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.arrListing = result
            self.tblView.reloadData()
        }
        
    }
    
    @objc func openMenu(sender:UIButton){
        print("openMenu=== \(sender.tag)")
        PKSAlertController.actionSheet(APP_NAME, message: "Please select option", sourceView: self.view, buttons: ["Edit","Delete","Invite Marker","Cancel"]) { (alert, index) in
            print("index == \(index)")
            if index == 2 {
                let vc = mainstoryBoard.instantiateViewController(withIdentifier: "TeacherListViewController") as?
                TeacherListViewController
                vc?.jobID = "\(self.arrListing[sender.tag]["id"] as? Int ?? 0)"
                self.navigationController?.pushViewController(vc!, animated: true)
            }
            if index == 1{
                self.deleteAssignment(jobID: (self.arrListing[sender.tag]["id"] as? Int ?? 0))
            }
            
        }
    }
    
    func deleteAssignment(jobID:Int){
        PKSAlertController.alert(APP_NAME, message: "Are you sure you want to delete this assignment?", buttons: ["YES" , "NO"]) { (alert, index) in
            if index == 0{
                let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.AssignmentDetail.rawValue + "\(jobID)/")
                let key = AppHelper.userDefaultForKey(key: "Key")
                let header = ["Authorization" : "Token \(key)"]
                PCNetworkManager.sharedInstance.hitDeleteService(url: url!, params: [:], header: header) { (response) in
                    guard response != nil ,
                        let message = response!["message"] as? String ,
                        message == "Job deleted successfully" else {
                            PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                            return
                    }
                    self.getResponselist(limit:0)
                }
                
            }
        }
        
    }
    
    
    
}


extension AssignmentListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListing.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "AssignmentTableViewCell", for: indexPath) as? AssignmentTableViewCell else {
            return UITableViewCell()
        }
        cell.btnOptions.tag = indexPath.item
        cell.btnOptions.addTarget(self, action: #selector(openMenu(sender:)), for: .touchUpInside)
        cell.setData(dict: arrListing[indexPath.item])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = mainstoryBoard.instantiateViewController(withIdentifier: "CreatedAssignmentDetailViewController") as? CreatedAssignmentDetailViewController
        vc?.id = arrListing[indexPath.item]["id"] as? Int ?? 0
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    
}
