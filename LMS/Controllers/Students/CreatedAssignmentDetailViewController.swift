//
//  CreatedAssignmentDetailViewController.swift
//  LMS
//
//  Created by Wemonde on 10/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class CreatedAssignmentDetailViewController: UIViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    var id = 0
    
    var dictData = [String:AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getResponseData(limit: 0)
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "DetailAssignmentTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailAssignmentTableViewCell")
        tblView.register(UINib.init(nibName: "AssignmentFilesTableViewCell", bundle: nil), forCellReuseIdentifier: "AssignmentFilesTableViewCell")

        
        
    }
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- server
    func  getResponseData(limit:Int){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.AssignmentDetail.rawValue + "\(id)/")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            print(response)
            guard response != nil ,
                let result = response as? [String:AnyObject] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            
            self.dictData = result;
            self.tblView.reloadData()
            print(self.dictData)
            
            
            
        }
        
    }
    
    
    
}

extension CreatedAssignmentDetailViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.item == 1{
            guard let cellLinks  = tableView.dequeueReusableCell(withIdentifier: "AssignmentFilesTableViewCell", for: indexPath) as? AssignmentFilesTableViewCell else {
                return UITableViewCell()
            }
            cellLinks.setData(dict: dictData)
            cellLinks.selectionStyle = .none
            return cellLinks

        }
        
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "DetailAssignmentTableViewCell", for: indexPath) as? DetailAssignmentTableViewCell else {
            return UITableViewCell()
        }
        cell.setData(dict: dictData)
        cell.selectionStyle = .none
        return cell
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 1{
            return 300
        }
        return UITableView.automaticDimension
    }
    
    
    
    
    
}






