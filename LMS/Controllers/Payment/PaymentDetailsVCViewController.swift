//
//  PaymentDetailsVCViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 28/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class PaymentDetailsVCViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var personalDetail: UIButton!
    @IBOutlet weak var bankDetail: UIButton!

    var tabSelected = "PersonalDetail"
    
    var personalData =  [[String:Any]]()
    var bankingData =  [[String:Any]]()
        
    var bankDetailView: BankDetailView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getPersonalData()
    }
    
    func getPersonalData() {
        
       let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.getPersonalDetail.rawValue)
       var header = [String:String]()
       
       let key = AppHelper.userDefaultForKey(key: "Key")
       header = ["Authorization" : "Token \(key)"]
       
       showActivityIndicator(decision: true, inViewConroller: self, animated: true)

       PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
        showActivityIndicator(decision: false, inViewConroller: self, animated: true)
           //removeAllIndicators(inViewConroller: self)
           guard response != nil ,
               let result = response!["legal_entity"] as? [String:Any] else {
                   PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                   return
           }
    
           self.personalData = [result]
           self.tblView.reloadData()
       }
    }
    
    func savePerosnalData() {
    }
    
//    func createProfile(params: [String: Any]) {
//        
//        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.inviteNewStudentToClass.rawValue)!
//
//        let key = AppHelper.userDefaultForKey(key: "Key")
//        let header = ["Authorization" : "Token \(key)"]//;charset=UTF-8
//        
//        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
//        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
//            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
//            guard response != nil ,
//                let key = response!["error"] as? Int else {
//                    if response!["error"] as? Int != 1 {
//                        self.addTeacherView.isHidden = true
//                        self.clearAllTextFields()
//                        showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
//                        
//                    } else {
//                        showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
//                    }
//                    return
//            }
//
//            if key != 1 {
//                self.addStudentView.isHidden = true
//                self.clearAllTextFields()
//                showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
//                
//            } else {
//                showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
//            }
//
//        }
//        
//    }
    

    func getBankProfile() {}
    
    func updateBankProfile() {}
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PaymentDetailsVCViewController : UITableViewDelegate,UITableViewDataSource{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if tabSelected == "PersonalDetail" {
            return personalData.count
        } else {
            return bankingData.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if tabSelected == "PersonalDetail" {

            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "PersonalDetailCell", for: indexPath) as? PersonalDetailCell else {
                return UITableViewCell()
            }
            cell.setData(dict: personalData[indexPath.item])
            cell.selectionStyle = .none
            return cell
        } else {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "BankDetailCell", for: indexPath) as? BankDetailCell else {
                return UITableViewCell()
            }
            cell.setData(data: bankingData[indexPath.item])
           
            cell.selectionStyle = .none
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
