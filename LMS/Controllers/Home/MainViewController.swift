//
//  MainViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class MainViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgViewBtn: UIButton!

    var parentView:UIViewController!
    var dataArray : [[String:Any]] = []
    var selectedTab = "Classes"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        selectedTab = "Classes"
        getResponseForProfile()
        initialSetup()
        getIntitalData()
        
        
    }
    
    func initialSetup() {
        
        collectionView.register(UINib(nibName: "MainCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MainCollectionCell")
        collectionView.register(UINib(nibName: "ClassesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ClassesCollectionViewCell")
        
//        dataArray = [
//            ["title":"My Jobs","image":"Image"],
//            ["title":"My Application","image":"notes"],
//            ["title":"Classes","image":"prev exam"],
//            ["title":"Profile Detail","image":"prev exam"]
//
//            ]
//
//        self.collectionView.reloadData()
        
    }
    
    @IBAction func btnProfileClicked(sender:UIButton){
        let profileVC = mainstoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        self.navigationController?.pushViewController(profileVC!, animated: true)
        
    }
    
    @IBAction func btnLogoutClicked(sender:UIButton){
        PKSAlertController.alert(APP_NAME, message: "Are you sure you want to logout? ", buttons: ["YES","NO"]) { (alert, index) in
            if index == 0{
                if IS_Google_LoggedIn {
                    GoogleLogin.sharedInstance.signout()
                    IS_Google_LoggedIn = false
                }
                appDelegate.toMoveLoginPage()
            }else{
                
            }
        }
    }
    
    @IBAction func btnClassesClicked(sender:UIButton) {
        selectedTab = "Classes"
        getResponseClasslist()
    }
    
    @IBAction func btnMarkingPlaceClicked(sender:UIButton) {
        selectedTab = "Marking Place"
        dataArray = [
           // ["title":"Latest Jobs","image":"prev exam"],
            ["title":"My Jobs","image":"Image"],
            ["title":"My Application","image":"notes"],
            ["title":"Profile Detail","image":"prev exam"]
        ]
        collectionView.reloadData()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("mainvc")
    }
    
    
    //MARK:- server
    
    func  getResponseClasslist(){
       // var url = URL.init()
        var url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.GetClasses.rawValue)
        
        if APP_FOR == "Teacher" {
            url = URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.GetAllClassTeacher.rawValue)
        }
        
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            
            guard response != nil ,
                let result = response!["results"] as? [[String:Any]] else {
                    
                    if response != nil {
                        
                        self.dataArray = response as! [[String : Any]]
                        self.collectionView.reloadData()
                    } else {
                        
                        PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                    }
                    
                    return
            }
            
            self.dataArray = result
            self.collectionView.reloadData()
            
        }
        
    }
    func  getResponseForProfile(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.profile.rawValue)
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)

        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)

            guard response != nil ,
                let result = response as? [String:AnyObject],
                let _ = result["first_name"] as? String,
                let _ = result["last_name"] as? String,
                let userType = result["user_type"] as? String else {
                   // PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            APP_FOR = userType
            AppHelper.setDictToUserDefault(dict: result, key: "UserData")
            self.getResponseClasslist()
        }
        
    }
    
    func  getIntitalData(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.InitialData.rawValue)
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            guard response != nil ,
                let result11 = response as? [String:AnyObject]else {
                   // PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            print("result === \(result11)")
            AppHelper.setDictToUserDefault(dict: result11, key: "IntialData")

        }
    }
}

extension MainViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    // MARK: - CollectionView Protocol
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{

        return dataArray.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if selectedTab == "Marking Place" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainCollectionCell", for: indexPath) as! MainCollectionCell
            let dict  = dataArray[indexPath.row]
            cell.setUpData(data: dict)
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClassesCollectionViewCell", for: indexPath) as! ClassesCollectionViewCell
            let dict  = dataArray[indexPath.row]
            cell.setData(dict: dict)
            
            return cell
            
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedTab == "Classes" {
            let homePageVC = mainstoryBoard.instantiateViewController(withIdentifier: "AllClassesViewController") as! AllClassesViewController
            let dict  = dataArray[indexPath.row]
            homePageVC.classId = "\(dict["id"] ?? "")"
            self.navigationController?.pushViewController(homePageVC, animated: true)
            
        } else {
            switch indexPath.row {
                
            case 0:
                //My jobs
                let homePageVC = mainstoryBoard.instantiateViewController(withIdentifier: "AssignmentListViewController") as! AssignmentListViewController
                self.navigationController?.pushViewController(homePageVC, animated: true)
            
            case 1:
                //My Applications
                let homePageVC = mainstoryBoard.instantiateViewController(withIdentifier: "ApplicationViewController") as! ApplicationViewController
                self.navigationController?.pushViewController(homePageVC, animated: true)
                
            case 2:
                // Profile
                let homePageVC = mainstoryBoard.instantiateViewController(withIdentifier: "MainProfileViewController") as! MainProfileViewController
                self.navigationController?.pushViewController(homePageVC, animated: true)
                
            default:
                break
            }
        }
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if selectedTab == "Classes" {
            return CGSize(width: (self.view.frame.width/2-10), height: self.view.frame.width/2-10)
        }
        return CGSize(width: (self.view.frame.width), height: self.collectionView.frame.height/5)
        
    }
}
