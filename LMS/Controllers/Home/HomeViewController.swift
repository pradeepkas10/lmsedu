//
//  HomeViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    
    var mainViewController: MainViewController!
    var assignmentDetailViewController: AssignmentDetailViewController!
    var activeExamsViewController: ActiveExamsViewController!
    var notificationsViewController: NotificationsViewController!
    var profileViewController: ProfileViewController!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var bottomView: UIView!

    @IBOutlet var buttons: [UIButton]!
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mainViewController = mainstoryBoard.instantiateViewController(withIdentifier: "MainViewController") as? MainViewController
        mainViewController.parentView = self
        assignmentDetailViewController = mainstoryBoard.instantiateViewController(withIdentifier: "AssignmentDetailViewController") as? AssignmentDetailViewController
        activeExamsViewController = mainstoryBoard.instantiateViewController(withIdentifier: "ActiveExamsViewController") as? ActiveExamsViewController
        notificationsViewController = mainstoryBoard.instantiateViewController(withIdentifier: "NotificationsViewController") as? NotificationsViewController
        profileViewController = mainstoryBoard.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
        
        viewControllers = [assignmentDetailViewController,activeExamsViewController,mainViewController,notificationsViewController,profileViewController]
        
        buttons[selectedIndex].isSelected = true
        tabBarSelected(buttons[selectedIndex])
        
        buttons[2].layer.cornerRadius = 55/2
        bottomView.addShadowTo()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("homevc")
    }
    
    @IBAction func tabBarSelected(_ sender: UIButton) {
        let previousIndex = selectedIndex
        selectedIndex = sender.tag
        
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParent: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParent()
        sender.isSelected = true
        
        let vc = viewControllers[selectedIndex]
        addChild(vc)
        vc.view.frame = contentView.bounds
        contentView.addSubview(vc.view)
        vc.didMove(toParent: self)
        self.view.bringSubviewToFront(vc.view)

    }
    
    

    
    
    
    
    

}

