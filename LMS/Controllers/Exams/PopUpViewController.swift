//
//  PopUpViewController.swift
//  LMS
//
//  Created by Wemonde on 16/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

@available(iOS 11.0, *)
let colorApp = UIColor.init(named: "2E85CC")


class PopUpViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    var selection  :((Int)->Void)!
    var dataSource = [[String:AnyObject]]()
    

    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(PopUpTableViewCell.self, forCellReuseIdentifier: "TeachersTableViewCell")
        
    }
    
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        selection(-1)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
        // self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}


extension PopUpViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "PopUpTableViewCell", for: indexPath) as? PopUpTableViewCell else {
            return UITableViewCell()
        }
        
        cell.mainView.isHidden = true
        cell.lblName.textColor = UIColor.lightGray
        let dict = dataSource[indexPath.item]
        cell.lblName.text = dict["name"] as? String
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70//UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? PopUpTableViewCell
        //cell?.lblName.textColor = colorApp
        cell?.mainView.isHidden = false
        selection(indexPath.item)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
}



class PopUpTableViewCell:UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblName: UILabel!

    
    
}
