//
//  UploadExamViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import AVFoundation

class UploadExamViewController: UIViewController {
    
    @IBOutlet weak var txtView:UITextView!
    @IBOutlet weak var lblStatus:UILabel!
    @IBOutlet weak var btnUploadFiles:UIButton!
    @IBOutlet weak var btnSubmit:UIButton!
    var responseHandler : ((String?) -> Void)!
    var textUploaded = ""

    var dataSource = [String:Any]()

    let ocr = CognitiveServices.sharedInstance.ocr
    let reconizeText = CognitiveServices.sharedInstance.reconizeText
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if textUploaded != ""{
            self.txtView.text = textUploaded
        }else{
            txtView.text = "Please paste the exam data or just upload the pictures."
            txtView.textColor = UIColor.lightGray
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btnUploadFiles.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
        btnSubmit.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)

    }

    
    @IBAction func btnSubmitClicked(sender:UIButton){
        if (txtView.text.isEmpty) || (txtView.text == "Please paste the exam data or just upload the pictures."){
            PKSAlertController.alert(APP_NAME, message: "Please enter some data to upload.")
            return
        }
       // writingDataToFile()
        responseHandler(txtView.text)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnBackClicked(sender:UIButton){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUploadFilesClicked(sender:UIButton){
        let photo = PhotoPicker.sharedInstance
        photo.initialize(self)
        photo.responseHandler  = { [weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            self!.getImageText(img: repsonse!)
        }
        

        
    }
    
    
    
    func getImageText(img:UIImage){
        
        guard let imgData = img.jpegData(compressionQuality: 0.60) else {
            return
        }
        
        print("size of image in KB: %f ", Double(imgData.count) / 1024.0)
        print("size of image in MB: %f ", Double(imgData.count) / 1024.0 / 1024)
        
        
        //        let requestObject: OCRRequestObject = (resource: imgData, language: .Automatic, detectOrientation: true)
        //        try! ocr.recognizeCharactersWithRequestObject(requestObject, completion: { (response) in
        //            if (response != nil){
        //                print("response == \(response!)")
        //                let text = self.ocr.extractStringFromDictionary(response!)
        //                self.txtView.text = text
        //            }
        //        })
        
        self.lblStatus.text = "Processing..."
        
        
        try! reconizeText.recognizeCharactersWithRequestObject(imgData) { (responseDict,responseHttp ) in
            if (responseDict != nil){
                print("response == \(responseDict!)")
                let text = self.reconizeText.extractLine(dict: responseDict!)
                self.txtView.text = text
            }
            if (responseHttp != nil){
                guard let response11 =  responseHttp ,
                    let statusCode = response11.statusCode as Int?,
                    statusCode == 202 else{
                        self.lblStatus.text = ""
                        PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                        return
                }
                
                guard let OperationLocation = response11.allHeaderFields["Operation-Location"] as? String else{return}
                print("OperationLocation == \(OperationLocation)")
                
                self.toGetResultFromAPI(strURL: OperationLocation ,count:1)
                
                
            }
        }
        
        
        
    }
    
    
    func toGetResultFromAPI(strURL:String,count:Int){
        print("calling calling")
        
        try! self.reconizeText.getTextFromHeaderURL(strURL, completion: { (result) in
            if (result == nil){
                return
            }
            
            guard let sucess =  result!["status"] as? String ,
                sucess == "Succeeded" else {
                    self.lblStatus.text = result!["status"] as? String
                    if count < 4{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            print("calling calling")
                            self.toGetResultFromAPI(strURL: strURL,count: count+1)
                        }
                    }
                    return
            }
            
            if (result != nil){
                print("response == \(result!)")
                self.lblStatus.text = ""
                let text = self.reconizeText.extractLine(dict: result!)
                if self.txtView.text == "Please paste the exam data or just upload the pictures."{
                    self.txtView.textColor = UIColor.darkGray
                    self.txtView.text = text
                }else{
                    self.txtView.text = self.txtView.text + "\n" + text
                }
            }
            
        })
        
        
    }
    
    
    
    func UploadTest(){
        
        let file = "file.txt"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                let data = Data(text2.utf8)
                
                let params = self.dataSource

                let url = ServicesURLs.BaseUrl.rawValue + ServiceMethods.AssignmentDetail.rawValue
                
                PCNetworkManager.sharedInstance.requestWith(endUrl: url, files: [data], parameters: params, onCompletion: { (respone) in
                    guard let idA = respone!["id"] as? Int else{
                         PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                        return
                    }
                    print("datat \(respone!)")
                    let vc = mainstoryBoard.instantiateViewController(withIdentifier: "TeacherListViewController") as? TeacherListViewController
                    vc?.jobID = "\(idA)"
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                }) { (error) in
                    print("error \(error!)")
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                }
                
            }
            catch let error {
                PKSAlertController.alert(APP_NAME, message: error.localizedDescription)
            }
        }
        
    }
    
    func writingDataToFile(){
        let file = "file.txt" //this is the file. we will write to and read from it
        let text = txtView.text ?? ""
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            //writing
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                self.UploadTest()
            }
            catch let error {
                print("error === \(error.localizedDescription)")
            }
        }
    }

    
    
    
}

extension UploadExamViewController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please paste the exam data or just upload the pictures."
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text.contains(UIPasteboard.general.string ?? "") {
            if textView.text == "Please paste the exam data or just upload the pictures."{
                textView.text = ""
                textView.textColor = UIColor.darkGray
            }
        }
        return true
    }

}




