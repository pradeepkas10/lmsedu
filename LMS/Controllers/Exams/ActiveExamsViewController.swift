//
//  ActiveExamsViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ActiveExamsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    
    
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "ActiveExamTableViewCell", bundle: nil), forCellReuseIdentifier: "ActiveExamTableViewCell")
        
    }
    
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
        // self.navigationController?.popViewController(animated: true)
    }
    
    
    
}


extension ActiveExamsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "ActiveExamTableViewCell", for: indexPath) as? ActiveExamTableViewCell else {
            return UITableViewCell()
        }        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}


