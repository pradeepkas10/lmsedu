//
//  AssignmentDetailViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import MobileCoreServices

class AssignmentDetailViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var txtLabel: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var btnTime: UIButton!
    @IBOutlet weak var txtDetails: UITextField!
    @IBOutlet weak var txtServices: UITextField!
    @IBOutlet weak var txtAssignmentName: UITextField!
    var textToUpload: String = ""

    @IBOutlet weak var viewJobs: UIView!
    @IBOutlet weak var viewRubic: UIView!

    var dataSourceLevel = [[String:AnyObject]]()
    var dataSourceSubject =  [[String:AnyObject]]()
    
    var selectedLevel = ""
    var selectedSubject = ""
    var selectedDate = ""
    var selctedService = ""
    
    var priceList = [[String:AnyObject]]()//["Marking (£10.00)" , "Proof Reading (£25.00)", "Moderation(£5.00)", "Editing(£3.00)"]
    
    
    var datePicker = UIDatePicker()
    var txtTime = UITextField()
    var viewPicker  = UIPickerView()
    
    
    //MARK:- Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.datePickerMode = .date
        view.addSubview(txtTime)
        txtTime.delegate = self
        txtTime.inputView = datePicker
        viewPicker.dataSource = self
        viewPicker.delegate = self
        txtServices.inputView = viewPicker
        
        
        if let data = AppHelper.getDictFromUserDefault(key: "IntialData") as? [String:AnyObject] {
            dataSourceLevel = data["levels"] as! [[String:AnyObject]]
            dataSourceSubject = data["subjects"] as! [[String:AnyObject]]
            priceList = data["services"] as! [[String:AnyObject]]
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btnNext.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }
    
    
    @IBAction func btnTimeUploadClicked(sender:UIButton){
        txtTime.becomeFirstResponder()
                
    }
    
    @IBAction func btnNextClicked(sender:UIButton){
        if (textToUpload == "") || (textToUpload == "Please paste the exam data or just upload the pictures."){
            PKSAlertController.alert(APP_NAME, message: "Please enter some data to upload.")
            return
        }
        
        if validation(){
            writingDataToFile()
        }
    }

    @IBAction func btnJobsFilesClicked(sender:UIButton){
        let vc = mainstoryBoard.instantiateViewController(withIdentifier: "UploadExamViewController") as? UploadExamViewController
        vc?.textUploaded = textToUpload
        vc?.responseHandler = {[weak self] response in
            guard response != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            print("text to uplod ==== \(response!)")
            self?.viewJobs.layer.borderColor = UIColor.init(red: 46/255, green: 135/255, blue: 209/255, alpha: 1.0).cgColor
            self?.viewJobs.layer.borderWidth = 1.0
            self!.textToUpload = response!
        }
        
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    
    @IBAction func btnRubicFilesClicked(sender:UIButton){
        let photo = PhotoPicker.sharedInstance
        photo.initialize(self)
        photo.responseHandler  = { [weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            self?.viewRubic.layer.borderColor = UIColor.init(red: 46/255, green: 135/255, blue: 209/255, alpha: 1.0).cgColor
            self?.viewRubic.layer.borderWidth = 1.0

        }

    }

    
    //prepare segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Level"{
            let destination = segue.destination as? PopUpViewController
            destination?.dataSource = dataSourceLevel
            destination?.selection = { (value) in
                print("selected== \(value)")
                if value == -1{
                    return
                }

                let dict = self.dataSourceLevel[value]
                self.txtLabel.text = dict["name"] as? String
                self.selectedLevel = "\(dict["id"] as? Int ?? 0)"
            }
        }
        
        if segue.identifier == "Subject"{
            let destination = segue.destination as? PopUpViewController
            destination?.dataSource = dataSourceSubject
            destination?.selection = { (value) in
                if value == -1{
                    return
                }
                let dict = self.dataSourceSubject[value]
                self.txtSubject.text = dict["name"] as? String
                self.selectedSubject = "\(dict["id"] as? Int ?? 0)"
                print("selected== \(value)")
            }
        }
        
        
        
    }
    
    
    //MARK:- text field delegate
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtTime{
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            btnTime.titleLabel?.text = formatter.string(from: datePicker.date)
            let formatterServer = DateFormatter()
            formatterServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            selectedDate = formatterServer.string(from: datePicker.date)
            print("date == \(selectedDate)")
        }
        
        if textField == txtServices{
            let vi = viewPicker.selectedRow(inComponent: 0)
            let dict = priceList[vi]
            txtServices.text = (dict["name"] as? String ?? "") + "(£\(dict["price"] as? String ?? ""))"
            selctedService = "\(dict["id"] as? Int ?? 0)"
        }
    }

    
    func UploadTest(){
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        let file = "file.txt"
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                let data = Data(text2.utf8)
                
                let params = ["name":txtAssignmentName.text!,
                              "description": txtDetails.text!,
                              "level":selectedLevel,
                              "subject":selectedSubject,
                              "due":selectedDate,
                              "services":selctedService,
                              "number_of_teacher":"1"] as [String : Any]

                let url = ServicesURLs.BaseUrl.rawValue + ServiceMethods.AssignmentDetail.rawValue
                
                PCNetworkManager.sharedInstance.requestWith(endUrl: url, files: [data], parameters: params, onCompletion: { (respone) in
                    showActivityIndicator(decision: false, inViewConroller: self, animated: true)

                    guard let idA = respone!["id"] as? Int else{
                        PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                        return
                    }
                    print("datat \(respone!)")
                    let vc = mainstoryBoard.instantiateViewController(withIdentifier: "TeacherListViewController") as? TeacherListViewController
                    vc?.jobID = "\(idA)"
                    self.navigationController?.pushViewController(vc!, animated: true)
                    
                }) { (error) in
                    print("error \(error!)")
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                }
                
            }
            catch let error {
                PKSAlertController.alert(APP_NAME, message: error.localizedDescription)
            }
        }
        
    }
    
    func writingDataToFile(){
        let file = "file.txt" //this is the file. we will write to and read from it
        let text = textToUpload
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            //writing
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                self.UploadTest()
            }
            catch let error {
                print("error === \(error.localizedDescription)")
            }
        }
    }
    
    
    func validation()->Bool{
        
        if (((txtAssignmentName.text?.isEmpty)!) || ((txtDetails.text?.isEmpty)!) || ((selectedLevel == "")) || ((selectedSubject == "")) || ((selectedDate == "")) || ((selctedService == ""))){
            PKSAlertController.alert(APP_NAME, message: "Please fill all data.")
            return false
        }
        
        return true
    }

   

}


extension AssignmentDetailViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return priceList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = priceList[row]
        return (dict["name"] as? String ?? "") + "(£\(dict["price"] as? String ?? ""))"
    }
    
    
    
}


