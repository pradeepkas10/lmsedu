//
//  ApplicationViewController.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ApplicationViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var ongoingApplicationsBtn: UIButton!
    @IBOutlet weak var pastApplicationBtn: UIButton!
    
    var classId = ""
    var tabSelected = "Assignment"
    
    var arrListing = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.register(UINib.init(nibName: "ApplicationTableViewCell", bundle: nil), forCellReuseIdentifier: "ApplicationTableViewCell")
        
        getApplicationtResponselist(is_ongoing: true)
    }
    
    //MARK:- server
    func  getApplicationtResponselist(is_ongoing: Bool) {
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.ApplicationList.rawValue + "?is_ongoing=\(is_ongoing)" + "?limit=100&offset=0")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                    return
            }
            
            self.arrListing = result
            self.tblView.reloadData()
            
        }
        
    }
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func ongoingApplicationsClicked(sender:UIButton) {
        tabSelected = "Assignment"
        getApplicationtResponselist(is_ongoing: true)
    }
    
    @IBAction func pastApplicationsClicked(sender:UIButton) {
        tabSelected = "People"
        getApplicationtResponselist(is_ongoing: false)
    }
    
}


extension ApplicationViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
            return arrListing.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "ApplicationTableViewCell", for: indexPath) as? ApplicationTableViewCell else {
            return UITableViewCell()
        }
        cell.setUpData(data: arrListing[indexPath.item])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130//UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = mainstoryBoard.instantiateViewController(withIdentifier: "ApplicationTableViewCell") as? ApplicationTableViewCell
//        vc?.id = arrListing[indexPath.item]["pk"] as? Int ?? 0
//        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if tabSelected != "Assignment" {
//            guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StudentTeacherHeader") as? StudentTeacherHeader else {
//                return nil
//            }
//            header.viewSeprator.isHidden = true
//            if section == 0 {
//                // header.viewSeprator.isHidden = true
//                header.lblName.text = "Teachers"
//            } else {
//                header.lblName.text = "Students"
//            }
//            return header
//        }
//        return UIView()
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if tabSelected != "Assignment" {
//            return 50
//        }
//        return 0
//    }
//
    
}

