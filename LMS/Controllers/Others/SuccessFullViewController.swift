//
//  SuccessFullViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class SuccessFullViewController: UIViewController {

    @IBOutlet weak var btnTakeAnotherOne: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnTakeAnotherOne.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }
    
}
