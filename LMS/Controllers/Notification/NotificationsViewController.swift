//
//  NotificationsViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var arrListing = [[String:Any]]()

    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //getResponselist()
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "NotificationsTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationsTableViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getResponselist()
    }
    
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
        // self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- server
    func  getResponselist(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.NotificationList.rawValue)
        var header = [String:String]()
        
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String {
            header = ["Authorization" : "Token \(key)"]
        }
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.arrListing = result
            self.tblView.reloadData()
        }
        
    }
}


extension NotificationsViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrListing.count == 0{
            AppHelper.EmptyMessage(message: "There is no notification for now.", tableView: tblView)
            return 0
        }
        tableView.backgroundView = nil
        return arrListing.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "NotificationsTableViewCell", for: indexPath) as? NotificationsTableViewCell else {
            return UITableViewCell()
        }
        cell.setData(dict: arrListing[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
    
}
