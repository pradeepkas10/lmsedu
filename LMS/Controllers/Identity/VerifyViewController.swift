//
//  VerifyViewController.swift
//  LMS
//
//  Created by Wemonde on 28/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

enum currentView {
    case passwordChange
    case verifyEmail
    case forgotPassword
}

class VerifyViewController: UIViewController {

    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var mainLbl: UILabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!

    var currentVieww :currentView = currentView.verifyEmail

    var btnBackHidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnNext.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }

    
    //MARK:- button Actions
    @IBAction func btnNextClicked(sender:UIButton){
        switch currentVieww {
        case currentView.verifyEmail:
            verifyEmail()
        case currentView.passwordChange:
            changePassword()
        case currentView.forgotPassword:
            forgotPassword()
     }
    }
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupUI(){
        switch currentVieww {
        case currentView.verifyEmail:
           txt1.placeholder = "Enter Recieved Code"
           view2.isHidden = true
        case currentView.forgotPassword:
            txt1.placeholder = "Enter Registered Email"
            mainLbl.text = "Enter Registered Email"
            view2.isHidden = true
        case currentView.passwordChange:
            txt1.placeholder = "Enter old Password"
            txt2.placeholder = "Enter New Password"
            mainLbl.text = "Change Password"
            if btnBackHidden{
                btnBack.isHidden = true
            }

      }
        
    }
    
    func verifyEmail(){
        if validation(){
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.registrationEmailVerify.rawValue)
            let param = ["key" : txt1.text! ]
            self.getResponseFromServer(url: url!, params: param)
        }else{
            PKSAlertController.alert(APP_NAME, message: "Enter valid key.")
        }
    }
    
    func changePassword(){
        if validation(){
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.ChangePassword.rawValue)
            let param = ["old_password" : txt1.text!,"new_password1" : txt2.text!, "new_password2" : txt2.text! ]
            self.getResponseFromServer(url: url!, params: param)
        }else{
            PKSAlertController.alert(APP_NAME, message: "Enter valid key.")
        }
    }
    
    func forgotPassword(){
        if validation(){
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.forgotPassword.rawValue)
            let param = ["email" : txt1.text! ]
            self.getResponseFromServer(url: url!, params: param)
        }else{
            PKSAlertController.alert(APP_NAME, message: "Enter valid key.")
        }
    }

    
    
    //MARK:- Server
    func  getResponseFromServer(url:URL,params:[String:Any]){
        var header = [String:String]()
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String {
            header = ["Authorization" : "Token \(key)"]
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if response != nil {
                PKSAlertController.alert(APP_NAME, message: response!["detail"] as? String ?? "", buttons: ["Ok"], tapBlock: { (action, int) in
                    if currentView.forgotPassword == self.currentVieww {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                print("response === \(response!)")
            }
        }
    }
    
    
    func validation()->Bool{
        
        if ((txt1.text?.isEmpty)! || (txt2.text?.isEmpty)!){
            return false
        }
        
        return true
    }




}
