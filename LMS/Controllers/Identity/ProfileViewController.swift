//
//  ProfileViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var headerView: UIView!
    var userData = [String:AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib.init(nibName: "ProfileMainTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileMainTableViewCell")
        tblView.register(UINib.init(nibName: "ProfileDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfileDetailTableViewCell")
        
        if let data = AppHelper.getDictFromUserDefault(key: "UserData") as? [String:AnyObject]{
            userData = data
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("profvc")
    }
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogoutClicked(sender:UIButton){
        PKSAlertController.alert(APP_NAME, message: "Are you sure you want to logout? ", buttons: ["YES","NO"]) { (alert, index) in
            if index == 0{
                appDelegate.toMoveLoginPage()
            }else{
            
            }
        }
    }
    
    
}



extension ProfileViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.item == 0 {
            guard let testCell = tableView.dequeueReusableCell(withIdentifier: "ProfileMainTableViewCell", for: indexPath) as? ProfileMainTableViewCell else {
                return UITableViewCell()
            }
            testCell.selectionStyle = .none
            return testCell
        }
        
        guard let detailCell = tableView.dequeueReusableCell(withIdentifier: "ProfileDetailTableViewCell", for: indexPath) as? ProfileDetailTableViewCell else {
            return UITableViewCell()
        }
        detailCell.selectionStyle = .none
        detailCell.setData(index: indexPath, dict: userData)
        return detailCell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 0{
            return 0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 231
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 4{
            let ChangePass = mainstoryBoard.instantiateViewController(withIdentifier: "VerifyViewController") as? VerifyViewController
            ChangePass?.currentVieww = currentView.passwordChange
            self.navigationController?.pushViewController(ChangePass!, animated: true)
        }
    }

    
}
