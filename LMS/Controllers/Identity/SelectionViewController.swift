//
//  SelectionViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

class SelectionViewController: UIViewController {

    @IBOutlet weak var btnLetsGo: UIButton!
    var userType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnLetsGo.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }
    
    @IBAction func btnStudentClicked(sender:UIButton) {
        userType = "Student"
    }
    
    @IBAction func btnTeacherClicked(sender:UIButton) {
        userType = "Teacher"
    }
    
    @IBAction func btnLetsGoClicked(sender:UIButton) {
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.userRole.rawValue)
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String {
            let param = ["role" : userType, "token": "\(key)"]
            self.getResponseFromServer(url: url!, params: param)
        }
        
    }
    
    //MARK:- Server
    func  getResponseFromServer(url:URL,params:[String:Any]){
        var header = [String:String]()
//        if let key = AppHelper.userDefaultForKey(key: "Key") as? String {
//            header = ["Authorization" : "Token \(key)"]
//        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if response != nil {
                print("response === \(response!)")
//                let loginVC = mainstoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                appDelegate.toMoveToHomePage()
                //self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    
}
