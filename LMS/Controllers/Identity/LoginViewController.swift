//
//  LoginViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

class LoginViewController: UIViewController {
    
    @IBOutlet weak var btnLoginn: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    
    //MARK:- view controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnLoginn.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }
        
    //MARK:- button Actions
    
    @IBAction func btnSingUPClicked(sender:UIButton){
        let vc = mainstoryBoard.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController
        self.navigationController?.pushViewController(vc!, animated: true)
    }

    @IBAction func btnNextClicked(sender:UIButton){
        if validation(){
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.login.rawValue)
            let param = ["username" : txtEmail.text! , "password" : txtPassword.text!]
            self.getResponseFromServer(url: url!, params: param)
        }else{
            PKSAlertController.alert(APP_NAME, message: "Enter valid credentials.")
        }
    }
    
    @IBAction func btnGooleLoginClicked(sender:UIButton){
        let googleLogin = GoogleLogin.sharedInstance
        googleLogin.initializeLogin(presentVC: self)
        googleLogin.responseHandler = {[weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            print("repsonse === \(repsonse!)")
            IS_Google_LoggedIn = true
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.loginGoogle.rawValue)
            let param = ["access_token" : repsonse! , "code" : "1"] as [String : Any]//, "code" : "1"] as [String : Any]
            self?.getResponseFromServer(url: url!, params: param)
        }

    }
    
    @IBAction func btnFacbookClicked(sender:UIButton){
        let facebookLogin = FacebookLogin.sharedInstance
        facebookLogin.intializeFBLogin(controller: self)
        facebookLogin.responseHandler = {[weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            print("repsonse === \(repsonse!)")
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.loginFacebook.rawValue)
            let param = ["access_token" : repsonse! , "code" : "1"] as [String : Any]
            self!.getResponseFromServer(url: url!, params: param)
        }
    }
    
    @IBAction func btnForgotPassClicked(sender:UIButton){
        let forgotVC = mainstoryBoard.instantiateViewController(withIdentifier: "VerifyViewController") as? VerifyViewController
        forgotVC?.currentVieww = currentView.forgotPassword
        self.navigationController?.pushViewController(forgotVC!, animated: true)
    
    }

    
    
    //MARK:- server
    func  getResponseFromServer(url:URL,params:[String:Any]){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: [:]) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            guard response != nil ,
                 let key = response!["key"] as? String else {
                    if response!["code"] as? String == "user_role" {
                        PKSAlertController.alert(APP_NAME, message: "Confirm your email address for verification!")
                    } else {
                        PKSAlertController.alert(APP_NAME, message: errorMessage.credentialsProblem)
                    }
                    GoogleLogin.sharedInstance.signout()
                    IS_Google_LoggedIn = false
                    return
            }
            print("key === \(key)")
            AppHelper.saveToUserDefault(value: key as AnyObject, key: "Key")
            if let userType = response!["user_type"] as? String {
               
                if userType == "" {
                    
                     APP_FOR = "Student"
                    //appDelegate.toMoveToHomePage()
//                    let selectionVc = mainstoryBoard.instantiateViewController(withIdentifier: "SelectionViewController") as! SelectionViewController
//                    self.navigationController?.pushViewController(selectionVc, animated: true)
                } else {
                    
                    APP_FOR = userType
                    
                }
                appDelegate.toMoveToHomePage()
                //let sele
            } else {
                appDelegate.toMoveToHomePage()
//                let selectionVc = mainstoryBoard.instantiateViewController(withIdentifier: "SelectionViewController") as! SelectionViewController
//                self.navigationController?.pushViewController(selectionVc, animated: true)
            }
            
            
        }
    }
    
    
    func validation()->Bool{
        
        if ((txtEmail.text?.isEmpty)!){
            return false
        }
        
        if ((txtPassword.text?.isEmpty)!){
            return false
        }
        
        
        return true
    }
    
    
    
}


//url == https://api.progressay.mynividata.in/rest-auth/login/
//params === ["password": "progressay@123", "username": "er.gunjanmodi+teacher6@gmail.com"]
//SUCCESS: {
//    key = 33b6c640dd4f16b4e3a6658719c90bc83fdf2761;
//}
