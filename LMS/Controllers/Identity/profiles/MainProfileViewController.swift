//
//  MainProfileViewController.swift
//  LMS
//
//  Created by Wemonde on 24/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import PageMenu


class MainProfileViewController: UIViewController {

    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        
        return false
    }
    
    var pageMenu : CAPSPageMenu?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setUpViews()
    }
    
    
    @IBAction func backBtnClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setUpViews(){
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        let v1 = mainstoryBoard.instantiateViewController(withIdentifier: "UpdatedProfleViewController") as! UpdatedProfleViewController
        v1.parentVC = self
        v1.title = "PROFILE"
        
        controllerArray.append(v1)

        let v3 = mainstoryBoard.instantiateViewController(withIdentifier: "PaymentDetailsVCViewController") as! PaymentDetailsVCViewController
        v3.title = "PAYMENT"
        controllerArray.append(v3)
        
        
        let v2 = mainstoryBoard.instantiateViewController(withIdentifier: "FeaturesViewController") as! FeaturesViewController
        v2.title = "FEATURES"
       controllerArray.append(v2)
        
        let ChangePass = mainstoryBoard.instantiateViewController(withIdentifier: "VerifyViewController") as? VerifyViewController
        ChangePass?.currentVieww = currentView.passwordChange
        
        ChangePass?.btnBackHidden = true
        ChangePass?.title = "SECURITY"
        controllerArray.append(ChangePass!)
        
        let parameters: [CAPSPageMenuOption] = [
            .useMenuLikeSegmentedControl(true),
            .selectionIndicatorHeight(2.0),
            .menuHeight(50),
            .scrollMenuBackgroundColor(UIColor.init(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)),
            .selectionIndicatorColor(.red),
            .bottomMenuHairlineColor(.clear),
            .selectedMenuItemLabelColor(UIColor.init(red: 46/255, green: 135/255, blue: 209/255, alpha: 1.0)),
            .unselectedMenuItemLabelColor(UIColor.init(red: 46/255, green: 135/255, blue: 209/255, alpha: 1.0))
            
        ]
        
        print("array of controller ==== \(controllerArray)")
        
        // Initialize page menu with controller array, frame, and optional parameters
        var yAxis: CGFloat = 64.0
        if hasTopNotch {
            yAxis = 90.0//144.0
        }
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect.init(x: 0, y: yAxis, width: self.view.frame.width, height: self.view.frame.height-64), pageMenuOptions: parameters)
        
        pageMenu!.delegate = self

        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.view.addSubview(pageMenu!.view)

    }

    

}




extension MainProfileViewController : CAPSPageMenuDelegate{
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("willMoveToPage === \(index)")
    }
    
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("didMoveToPage === \(index)")
    }
    
}
