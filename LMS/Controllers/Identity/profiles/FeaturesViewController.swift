//
//  FeaturesViewController.swift
//  LMS
//
//  Created by Wemonde on 24/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class FeaturesViewController: UIViewController {
    
    @IBOutlet weak var btnSaveChanges: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var switchMarking: UISwitch!
    @IBOutlet weak var switchModeration: UISwitch!
    @IBOutlet weak var switchOnlineClass: UISwitch!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let data = AppHelper.getDictFromUserDefault(key: "UserData") as? [String:AnyObject]{
            if let marking = data["is_marking_place"] as? Bool{
                switchMarking.setOn(marking , animated: true)
            }
            if let is_moderation = data["is_moderation"] as? Bool{
                switchModeration.setOn(is_moderation , animated: true)
            }
            if let is_online_classroom = data["is_online_classroom"] as? Bool{
                switchOnlineClass.setOn(is_online_classroom , animated: true)
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnSaveChanges.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 25)
        
    }
    
    @IBAction func btnSaveClicked(sender:UIButton){
        getResponse()
    }
    
    //https://api.progressay.mynividata.in/
    
    //MARK:- server
    func  getResponse(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + "users/user-settings/")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        let param =  ["is_marking_place": switchMarking.isOn , "is_moderation": switchModeration.isOn, "is_online_classroom": switchOnlineClass.isOn]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPatchService(url: url!, params: param, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let result = response else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            PCNetworkManager.sharedInstance.updateProfileInUserDefault(handler: { (_) in
            })
            print("result ==== \(result)")
            
        }
        
        
    }
    
    
    
    
    
}
