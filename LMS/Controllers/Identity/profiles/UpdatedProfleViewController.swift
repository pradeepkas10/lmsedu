//
//  UpdatedProfleViewController.swift
//  LMS
//
//  Created by Wemonde on 24/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class UpdatedProfleViewController: UIViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var jobID = ""
    var userID = ""
    
    var arrListing = [[String:AnyObject]]()
    var dataSource = [String:AnyObject]()
    var inviteMarker : Bool = false
    var parentVC:UIViewController!

    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "TeacherDetailHeaderCell", bundle: nil), forCellReuseIdentifier: "TeacherDetailHeaderCell")
        tblView.register(UINib.init(nibName: "SubjectTableViewCell", bundle: nil), forCellReuseIdentifier: "SubjectTableViewCell")
        tblView.register(UINib.init(nibName: "TeacherReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "TeacherReviewTableViewCell")
        //getResponselist()
        if let data = AppHelper.getDictFromUserDefault(key: "UserData") as? [String:AnyObject]{
            dataSource = data
        }

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- server
    func  getResponselist(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.teacherDetails.rawValue + "\(userID)/")
        var header = [String:String]()
        
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String{
            header = ["Authorization" : "Token \(key)"]
        }
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response as? [String:AnyObject] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            print("result ==== \(result)")
            self.dataSource = result
            self.tblView.reloadData()
        }
        
    }
    
    
    
    
    @objc func btnInviteMarkerClicked(sender:UIButton){
        let vc = mainstoryBoard.instantiateViewController(withIdentifier: "ProfileEditViewController") as? ProfileEditViewController
        parentVC.navigationController?.pushViewController(vc!, animated: true)

    }
    
    
    
    
    
}


extension UpdatedProfleViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0{
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TeacherDetailHeaderCell", for: indexPath) as? TeacherDetailHeaderCell else {
                return UITableViewCell()
            }
            cell.btnMaker.addTarget(self, action: #selector(btnInviteMarkerClicked(sender:)), for: .touchUpInside)
            cell.isStudent = true
            cell.setData(dict: dataSource,inviteMarker:inviteMarker)
            cell.selectionStyle = .none
            return cell
        }
        if indexPath.item == 1 || indexPath.item == 2 || indexPath.item == 3{
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "SubjectTableViewCell", for: indexPath) as? SubjectTableViewCell else {
                return UITableViewCell()
            }
            cell.setData(dict: dataSource ,index:indexPath)
            cell.selectionStyle = .none
            return cell
        }
        
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TeacherReviewTableViewCell", for: indexPath) as? TeacherReviewTableViewCell else {
            return UITableViewCell()
        }
        cell.setDataSource(dataSource: dataSource)
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 4{
            if let reviews = dataSource["reviews"] as? [[String:AnyObject]]{
                return CGFloat(reviews.count * 120) + 50 //(reviews.count == 0 ? 0 : 50)
            }
            return 0
        }
        return UITableView.automaticDimension
    }
    
    
    
    
    
}
