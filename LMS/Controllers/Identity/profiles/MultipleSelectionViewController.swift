//
//  MultipleSelectionViewController.swift
//  LMS
//
//  Created by Wemonde on 25/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class MultipleSelectionViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblHeader: UILabel!

    var selection  :((String,[Int],txtType)->Void)!
    var dataSource = [[String:AnyObject]]()
    var type:txtType!
    
    var selectionIDArr = [Int]()
    var selectionNameArr = [String]()
    
    var selectionView:MultipleSelectionViewController!
    
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(PopUpTableViewCell.self, forCellReuseIdentifier: "TeachersTableViewCell")
        switch type {
        case .qulification? :
            lblHeader.text = "Qulifications"
        case .subject?:
            lblHeader.text = "Subjects"
        case .none:
            lblHeader.text = ""
        }

        
    }
    
    func setUpInitailData(_ data :[[String:AnyObject]],selectedArr:[Int] ){
        dataSource = data
//        selectionIDArr = [Int]()
        selectionNameArr = [String]()
        if tblView != nil{
            tblView.reloadData()
            switch type {
            case .qulification? :
                lblHeader.text = "Qulifications"
                selectionIDArr = selectedArr
            case .subject?:
                lblHeader.text = "Subjects"
                selectionIDArr = selectedArr
            case .none:
                lblHeader.text = ""
            }
        }
    }
    
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        selection(selectionNameArr.joined(separator: ","),selectionIDArr ,type)
        print(selectionNameArr)
        print(selectionIDArr)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
        // self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
}


extension MultipleSelectionViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "PopUpTableViewCell", for: indexPath) as? PopUpTableViewCell else {
            return UITableViewCell()
        }
        
        cell.mainView.isHidden = true
        cell.lblName.textColor = UIColor.lightGray
        let dict = dataSource[indexPath.item]
        cell.lblName.text = dict["name"] as? String
        
        if selectionIDArr.contains((dict["id"] as? Int)!){
            cell.mainView.isHidden = false
        }else{
            cell.mainView.isHidden = true
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70//UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? PopUpTableViewCell
        cell?.mainView.isHidden = false
        let dict = dataSource[indexPath.item]
        if !selectionIDArr.contains((dict["id"] as? Int)!){
            selectionIDArr.append((dict["id"] as? Int)!)
            selectionNameArr.append((dict["name"] as? String)!)
        }else{
            let indexOfID = selectionIDArr.firstIndex(of: (dict["id"] as? Int)!) // 0
            let indexOfName = selectionNameArr.firstIndex(of: (dict["name"] as? String)!)
            selectionIDArr.remove(at: indexOfID!)
            selectionNameArr.remove(at: indexOfName!)
        }
        
    }
    
    
    
    
}

