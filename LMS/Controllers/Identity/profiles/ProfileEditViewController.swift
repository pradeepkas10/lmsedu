//
//  ProfileEditViewController.swift
//  LMS
//
//  Created by Wemonde on 23/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

import AVFoundation


enum txtType{
    case qulification
    case subject
}

class ProfileEditViewController: UIViewController {
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnSaveChanges: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtBio: UITextField!
    @IBOutlet weak var txtRate: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtskills: UITextField!
    @IBOutlet weak var txtQualfication: UITextField!
    @IBOutlet weak var txtSubject: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    
    var selectionView:MultipleSelectionViewController!
    
    var userDropDownData = [String:AnyObject]()
    var userData = [String:AnyObject]()
    
    var dropDownPickerData = [[String:AnyObject]]()
    var viewPicker = UIPickerView()
    var selectedCounrtyCode = ""
    var selectedCityCode = ""
    var imagePicker = UIImagePickerController()
    var selectedQualification = [Int]()
    var selectedSubject = [Int]()


    //https://api.progressay.mynividata.in/rest-auth/user/ patch8
    override func viewDidLoad() {
        super.viewDidLoad()
        getInitialData()
        viewPicker.delegate = self
        viewPicker.dataSource = self
        txtCity.inputView = viewPicker
        txtCountry.inputView = viewPicker
        if let data = AppHelper.getDictFromUserDefault(key: "UserData") as? [String:AnyObject]{
            userData = data
            setData(dict: userData)
        }
        
        selectionView = mainstoryBoard.instantiateViewController(withIdentifier: "MultipleSelectionViewController") as? MultipleSelectionViewController
        selectionView.selection = { (selectionName , arrID , type) in
            print("arrr == \(arrID) \(type)")
            switch type {
            case txtType.qulification:
                self.txtQualfication.text =  selectionName
                self.selectedQualification  = arrID
            case txtType.subject:
                self.txtSubject.text =  selectionName
                self.selectedSubject = arrID
            }
            
        }

        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnAdd.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 15)
        btnRemove.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 15)
        btnSaveChanges.roundCorners(corners: [.topLeft,.bottomLeft,.topRight], radius: 25)
        
    }
    
    @IBAction func btnBackClicked(sender:UIButton){
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSaveChnages(sender:UIButton){
        self.saveData()
    }

    
    @IBAction func btnRemove(sender:UIButton){
        imgView.image = nil
        self.removeImage()
    }
    
    @IBAction func btnAdd(sender:UIButton){
        let photo = PhotoPicker.sharedInstance
        photo.initialize(self)
        photo.responseHandler  = { [weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            self!.imgView.image = repsonse
            self!.addImage(img: repsonse!.jpegData(compressionQuality: 0.5)!)
        }
        
    }
    

    
    @IBAction func btnCitiesClicked(sender:UIButton){
        getCities()
        
    }
    
    
    
    
    //MARK:- server
    func  getInitialData(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.userDropDown.rawValue)
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let result = response as? [String:AnyObject] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.userDropDownData = result
        }
        
    }
    
    func getCities(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.cityDropDown.rawValue + "?country=\(selectedCounrtyCode)&limit=10000")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let result = response as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.dropDownPickerData = result
            self.viewPicker.reloadAllComponents()
            self.txtCity.becomeFirstResponder()
        }
    }
    
    //Request URL: https://api.progressay.mynividata.in/

    func removeImage(){
        
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + "rest-auth/user/")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        let param =  ["avatar": nil] as [String : Any?]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPatchService(url: url!, params: param, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let result = response else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            PCNetworkManager.sharedInstance.updateProfileInUserDefault(handler: { (done) in
                if done{
                    self.setAndReloadData()
                }
            })
            print("result ==== \(result)")
            
        }

    }
    
    func addImage(img:Data){
        let url =  ServicesURLs.BaseUrl.rawValue + "rest-auth/user/"
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.uploadImage(endUrl: url, files: img, parameters: [:], withName: "avatar") { (repsonse) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            print("repsonse uplod iamge ==== \(repsonse)")
        }
        
    }
    
    
    func setAndReloadData(){
        if let data = AppHelper.getDictFromUserDefault(key: "UserData") as? [String:AnyObject]{
            userData = data
            setData(dict: userData)
        }
    }
    
    func setData(dict:[String:AnyObject]){
        if let FName = dict["first_name"] as? String{
            txtFirstName.text = FName
        }
        if let LName = dict["last_name"] as? String{
            self.txtLastName.text =  LName
        }
        
        if let title = dict["title"] as? String{
            self.txtTitle.text = title
        }
        if let city = dict["city"] as? [String:AnyObject]{
            self.txtCity.text = "\(city["name"] as? String ?? "")"
            self.selectedCityCode = "\(city["id"] as? Int64 ?? 0)"
        }
        if let country = dict["country"] as? [String:AnyObject]{
            self.txtCountry.text = "\(country["name"] as? String ?? "")"
            self.selectedCounrtyCode = "\(country["id"] as? Int64 ?? 0)"
        }
        
        if let bio = dict["bio"] as? String{
            self.txtBio.text = bio
        }
        
        if let price = dict["rate"] as? String{
            self.txtRate.text = price
        }
        
        if let skills = dict["skills"] as? String{
            self.txtskills.text = skills
        }
        
        selectedQualification = [Int]()
        var Q = [String]()
        if let arrQ = dict["qualifications"] as? [[String:AnyObject]]{
            for ele in arrQ{
                let id = ele["id"] as? Int ?? 0
                Q.append((ele["name"] as? String)!)
                selectedQualification.append(Int(id))
            }
        }
        
        selectedSubject = [Int]()
        var S = [String]()
        if let arrS = dict["subjects"] as? [[String:AnyObject]]{
            for ele in arrS{
                let id = ele["id"] as? Int ?? 0
                S.append((ele["name"] as? String)!)
                selectedSubject.append(Int(id))
            }
        }
        
        txtQualfication.text = Q.joined(separator: ",")
        txtSubject.text = S.joined(separator: ",")

        
        print("selected array === \(selectedCounrtyCode) \(selectedCityCode)")

        
        
        if let img = dict["avatar"] as? String{
            Alamofire.request(img).responseImage { response in
                debugPrint(response)
                if let image = response.result.value {
                    self.imgView.image = image
                    print("image downloaded: \(image)")
                }
            }
        }
        
    }
    
    
    func saveData() {
        
        var  dict = [String:Any]()
        dict["first_name"] = txtFirstName.text
        dict["last_name"] = txtLastName.text
        dict["city"] = selectedCityCode
        dict["country"] = selectedCounrtyCode
        dict["bio"] = txtBio.text
        dict["rate"] = txtRate.text
        dict["skills"] = txtskills.text
        dict["title"] = txtTitle.text
        
        var arrS = [[String:Any]]()
        for ele in selectedSubject{
            var dictS = [String:Any]()
            dictS["subject_id"] = "\(ele)"
            arrS.append(dictS)
        }
        dict["subjects"] = arrS
        
        var arrQ = [[String:Any]]()
        for ele in selectedQualification{
            var dictQ = [String:Any]()
            dictQ["qualification_id"] = "\(ele)"
            arrQ.append(dictQ)
        }
        dict["qualifications"] = arrQ

        print("final dict === \(dict)")
        

        let url = URL.init(string: ServicesURLs.BaseUrl.rawValue + "rest-auth/user/")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)

        PCNetworkManager.sharedInstance.hitPatchService(url: url!, params: dict, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let result = response else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            PCNetworkManager.sharedInstance.updateProfileInUserDefault(handler: { (done) in
                if done{
                    self.setAndReloadData()
                }
            })
            print("result ==== \(result)")
        }
    }
    
    
    
}


extension ProfileEditViewController : UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if txtQualfication == textField{
            if let data = userDropDownData["qualifications"] as? [[String:AnyObject]]{
                textField.resignFirstResponder()
                selectionView.dataSource = data
                selectionView.selectionIDArr = selectedQualification
                selectionView.type = txtType.qulification
                selectionView.setUpInitailData(data, selectedArr: selectedQualification)
                self.navigationController?.present(selectionView, animated: true, completion: nil)
            }
        }
        if txtSubject == textField{
            if let data = userDropDownData["subjects"] as? [[String:AnyObject]]{
                textField.resignFirstResponder()
                selectionView.dataSource = data
                selectionView.type = txtType.subject
                selectionView.setUpInitailData(data, selectedArr: selectedSubject)
                self.navigationController?.present(selectionView, animated: true, completion: nil)
            }
        }
        
        if txtCountry == textField{
            dropDownPickerData = userDropDownData["countries"] as! [[String : AnyObject]]
            viewPicker.reloadAllComponents()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if txtCountry == textField{
            let index = viewPicker.selectedRow(inComponent: 0)
            if let county = userDropDownData["countries"] as? [[String : AnyObject]]{
                let dict = county[index]
                txtCountry.text = dict["name"] as? String ?? ""
                self.selectedCounrtyCode = "\(dict["id"] as? Int64 ?? 0)"
            }
        }
        
        if txtCity == textField{
            let index = viewPicker.selectedRow(inComponent: 0)
            let dict = dropDownPickerData[index]
            txtCity.text = dict["name"] as? String ?? ""
            selectedCityCode = "\(dict["id"] as? Int64 ?? 0)"
        }
        
    }
    
    
}



extension ProfileEditViewController : UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dropDownPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = dropDownPickerData[row]
        return dict["name"] as? String ?? ""
    }
    
    
    
}



