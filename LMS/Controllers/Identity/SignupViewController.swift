//
//  SignupViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 07/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

class SignupViewController: UIViewController {
    
    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        btnSignup.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 25.0)
    }
    
    //MARK:- button Actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btnNextClicked(sender:UIButton){
        if validation(){
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.registration.rawValue)
            let param = [ "email" : txtEmail.text! ,
                          "username" : txtUserName.text! ,
                          "password1" : txtPassword.text!,
                          "password2" : txtPassword.text!,
                          "first_name" : txtFirstName.text!,
                          "last_name" : txtLastName.text!,
                          "newsletter" : true
                ] as [String : Any]
            
            self.getResponseFromServer(url: url!, params: param)
            
        }else{
            PKSAlertController.alert(APP_NAME, message: "Enter valid credentials.")
        }
    }
    
    
    @IBAction func btnGooleLoginClicked(sender:UIButton){
        let googleLogin = GoogleLogin.sharedInstance
        googleLogin.initializeLogin(presentVC: self)
        googleLogin.responseHandler = {[weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            print("repsonse === \(repsonse!)")
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.loginGoogle.rawValue)
            let param = ["access_token" : repsonse! , "code" : "1"] as [String : Any]
            self!.getResponseFromServer(url: url!, params: param)
        }
        
    }
    
    @IBAction func btnFacbookClicked(sender:UIButton){
        let facebookLogin = FacebookLogin.sharedInstance
        facebookLogin.intializeFBLogin(controller: self)
        facebookLogin.responseHandler = {[weak self] (repsonse) in
            guard repsonse != nil else {
                PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                return
            }
            print("repsonse === \(repsonse!)")
            let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.loginFacebook.rawValue)
            let param = ["access_token" : repsonse! , "code" : "1"] as [String : Any]
            self!.getResponseFromServer(url: url!, params: param)
        }
    }
    
    
    
    func  getResponseFromServer(url:URL,params:[String:Any]){
        MBProgressHUD.showAdded(to: self.view, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: [:]) { (response) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard response != nil, let detail = response!["detail"] as? String else{
                if let arr = response!["password1"] as? [[String:Any]] {
                    if let dict = arr[0] as? [String:Any] {
                        PKSAlertController.alert(APP_NAME, message: dict["message"] as? String ??  "\(errorMessage.credentialsProblem)")
                        return
                    }
                }
                
                
                PKSAlertController.alert(APP_NAME, message: errorMessage.credentialsProblem)
                return
            }
            
            PKSAlertController.alert(APP_NAME, message: "Please verify the link sent over email", buttons: ["Okay"], tapBlock: { (action, int) in
                
                let vc = mainstoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as?
                LoginViewController
                
                self.navigationController?.pushViewController(vc!, animated: true)
                
            })
            //
            print("response === \(response!)")
        }
    }
    
    
    
    
    func validation()->Bool{
        
        if ((txtFirstName.text?.isEmpty)!){
            return false
        }
        if ((txtLastName.text?.isEmpty)!){
            return false
        }
        if ((txtEmail.text?.isEmpty)!){
            return false
        }
        if ((txtUserName.text?.isEmpty)!){
            return false
        }
        if ((txtPassword.text?.isEmpty)!){
            return false
        }
        
        
        return true
    }
    
    
    
    //    {
    //    "username": "pradeep",
    //    "email": "pradeepkas10@gmail.com",
    //    "password1": "string",
    //    "password2": "string",
    //    "first_name": "pradeep",
    //    "last_name": "pradeep",
    //    "newsletter": true
    //    }
    
    
    
    
}
