//
//  TeacherDetailViewController.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

//Request URL: https://api.progressay.mynividata.in/market-place/check-job-application/
//    job_id: "13"
//    user_id: "25"
//    postt
//{"is_marker_invited":false}

//  Request URL: https://api.progressay.mynividata.in/users/user-public-profile/25/


class TeacherDetailViewController: UIViewController {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var jobID = ""
    var userID = ""
    
    var arrListing = [[String:AnyObject]]()
    var dataSource = [String:AnyObject]()
    var inviteMarker : Bool = false
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "TeacherDetailHeaderCell", bundle: nil), forCellReuseIdentifier: "TeacherDetailHeaderCell")
        tblView.register(UINib.init(nibName: "SubjectTableViewCell", bundle: nil), forCellReuseIdentifier: "SubjectTableViewCell")
        tblView.register(UINib.init(nibName: "TeacherReviewTableViewCell", bundle: nil), forCellReuseIdentifier: "TeacherReviewTableViewCell")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getResponselist()
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.jobStatus.rawValue)
        let param = ["user_id":userID, "job_id" :jobID]
        checkInviteStatus(url: url!, params: param)
    }
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- server
    func  getResponselist(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.teacherDetails.rawValue + "\(userID)/")
        var header = [String:String]()
        
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String{
            header = ["Authorization" : "Token \(key)"]
        }
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response as? [String:AnyObject] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            print("result ==== \(result)")
            self.dataSource = result
            self.tblView.reloadData()
        }
        
    }
    
    func  checkInviteStatus(url:URL,params:[String:Any]){
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: [:]) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let key = response!["is_marker_invited"] as? Bool else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.inviteMarker = key
            print("key === \(key)")
            self.tblView.reloadData()
            
        }
    }
    
    
    
    @objc func btnInviteMarkerClicked(sender:UIButton){
        PKSAlertController.alert(APP_NAME, message: "Are you sure you want to Hire? ", buttons: ["YES","NO"]) { (alert, index) in
            if index == 0{
                self.hireToMarker()
            }else{
                
            }
        }
    }
    
    
    func hireToMarker(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.selectMarker.rawValue)
        
        let param = ["markers":[userID] , "job" :jobID] as [String : Any]
        print(" param === \(param)")
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        let header = ["Authorization" : "Token \(key)"]//;charset=UTF-8
        
        print("header==== \(header)")
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        PCNetworkManager.sharedInstance.hitPostService(url: url!, params: param,header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            print("Marker response === \(response)")
            guard response != nil ,
                let message = response!["message"] as? String ,
                message == "Marker Hired !"
                else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            print("key === \(message)")
            
            PKSAlertController.alert(APP_NAME, message: "Marker hired successfully.")
            self.inviteMarker = true
            self.tblView.reloadData()
            
        }
    }
    
    
    
}


extension TeacherDetailViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item == 0{
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TeacherDetailHeaderCell", for: indexPath) as? TeacherDetailHeaderCell else {
                return UITableViewCell()
            }
            cell.btnMaker.addTarget(self, action: #selector(btnInviteMarkerClicked(sender:)), for: .touchUpInside)
            cell.setData(dict: dataSource,inviteMarker:inviteMarker)
            cell.selectionStyle = .none
            return cell
        }
        if indexPath.item == 1 || indexPath.item == 2 || indexPath.item == 3{
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "SubjectTableViewCell", for: indexPath) as? SubjectTableViewCell else {
                return UITableViewCell()
            }
            cell.setData(dict: dataSource ,index:indexPath)
            cell.selectionStyle = .none
            return cell
        }
        
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TeacherReviewTableViewCell", for: indexPath) as? TeacherReviewTableViewCell else {
            return UITableViewCell()
        }
        cell.setDataSource(dataSource: dataSource)
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item == 4{
            if let reviews = dataSource["reviews"] as? [[String:AnyObject]]{
                return CGFloat(reviews.count * 120) + 50 //(reviews.count == 0 ? 0 : 50)
            }
            return 0
        }
        return UITableView.automaticDimension
    }
    
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    
    
    
}
