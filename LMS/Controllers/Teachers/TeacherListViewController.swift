//
//  TeacherListViewController.swift
//  LMS
//
//  Created by Ishika Gupta on 08/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class TeacherListViewController: UIViewController {

    
    @IBOutlet weak var tblView: UITableView!
    
    var arrListing = [[String:AnyObject]]()

    var jobID = ""
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "TeachersTableViewCell", bundle: nil), forCellReuseIdentifier: "TeachersTableViewCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getResponselist()
    }
    
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
       // self.navigationController?.popViewController(animated: true)
    }

    
    //Request URL: https://progressay.mynividata.in/markers?jobId=3

    
    //MARK:- server
    func  getResponselist(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.TeacherList.rawValue + "?job_id=\(jobID)&limit=100&offset=0")
        var header = [String:String]()
        
        if let key = AppHelper.userDefaultForKey(key: "Key") as? String{
            header = ["Authorization" : "Token \(key)"]
        }
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)

        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)

            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: errorMessage.somethingWentWrng)
                    return
            }
            self.arrListing = result
            self.tblView.reloadData()
        }
        
    }


    
}


extension TeacherListViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListing.count;
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "TeachersTableViewCell", for: indexPath) as? TeachersTableViewCell else {
            return UITableViewCell()
        }
        cell.setData(dict: arrListing[indexPath.item])
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrListing[indexPath.item]
        let vc  = mainstoryBoard.instantiateViewController(withIdentifier: "TeacherDetailViewController") as? TeacherDetailViewController
        vc?.userID = "\(dict["id"] as? Int ?? 0)"
        vc?.jobID = jobID
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    
    
    
}
