//
//  TeacherFilterViewController.swift
//  LMS
//
//  Created by Wemonde on 16/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class TeacherFilterViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var viewHeader:FilterTeacherHeaderView!

    var arrHeader = ["Subject" , "Level" , "Price"]
    
    //MARK:- view controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.tableFooterView = UIView()
        tblView.register(UINib.init(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        btnApply.roundCorners(corners: [.topLeft, .topRight,.bottomLeft], radius: 20)
       // viewHeader = FilterTeacherHeaderView.fromNib()
        
        tblView.register(UINib.init(nibName: "FilterTeacherHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterTeacherHeaderView")

    }
    
    ///filter url Request URL: https://api.progressay.mynividata.in/market-place/marker-listing/?job_id=13&limit=10&offset=0&rate__range=68__167&subjects=2&subjects=1

    
    
    //MARK:- actions
    
    @IBAction func btnCancelClicked(sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnFilterClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}



extension TeacherFilterViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4;
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterTeacherHeaderView") as? FilterTeacherHeaderView else {
            return nil
        }
        header.viewSeprator.isHidden = false
        if section == 0{
            header.viewSeprator.isHidden = true
        }
         header.lblName.text = arrHeader[section]
        
        return header
    }


    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell  = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as? FilterTableViewCell else {
            return UITableViewCell()
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
    
    
}
