//
//  AllClassesViewController.swift
//  LMS
//
//  Created by Wemonde on 21/07/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class AllClassesViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var assignmentBtn: UIButton!
    @IBOutlet weak var peopleBtn: UIButton!
    @IBOutlet weak var addTeacherView: UIView!
    @IBOutlet weak var addStudentView: UIView!
    @IBOutlet weak var addAssignmentView: UIView!

    @IBOutlet weak var emailTeacher: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var studentEmail: UITextField!
    @IBOutlet weak var studentPassword: UITextField!
    
    @IBOutlet weak var assModule: UITextField!
    @IBOutlet weak var assName: UITextField!
    @IBOutlet weak var assTopic: UITextField!
    @IBOutlet weak var assDueDate: UITextField!

    var datePicker = UIDatePicker()
    var txtTime = UITextField()
    var viewPicker  = UIPickerView()
    var selectedDate = ""

    
    var classId = ""
    var tabSelected = "Assignment"
    
    var arrListing = [[String:AnyObject]]()
    var classStudentsList =  [[String:AnyObject]]()
    var classTeachersList =  [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if APP_FOR == "Student" {
            peopleBtn.isHidden = true
        } else {
            peopleBtn.isHidden = false
        }
        tblView.register(UINib.init(nibName: "ClassAssignmentTableViewCell", bundle: nil), forCellReuseIdentifier: "ClassAssignmentTableViewCell")
        tblView.register(UINib.init(nibName: "StudentTeacherHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "StudentTeacherHeader")
        tblView.register(UINib.init(nibName: "ClassPeopleTableViewCell", bundle: nil), forCellReuseIdentifier: "ClassPeopleTableViewCell")
   
        emailTeacher.setBottomBorder()
        firstName.setBottomBorder()
        firstName.setBottomBorder()
        lastName.setBottomBorder()
        studentEmail.setBottomBorder()
        studentPassword.setBottomBorder()
        
        assModule.setBottomBorder()
        assName.setBottomBorder()
        assTopic.setBottomBorder()
        assDueDate.setBottomBorder()
        
        getAssignmentResponselist()
        
        datePicker.datePickerMode = .date
        //view.addSubview(assDueDate)
        assDueDate.delegate = self
        assDueDate.inputView = datePicker
//        viewPicker.dataSource = self
//        viewPicker.delegate = self
    }
    

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == assDueDate {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            assDueDate.text = formatter.string(from: datePicker.date)
            let formatterServer = DateFormatter()
            formatterServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            selectedDate = formatterServer.string(from: datePicker.date)
            print("date == \(selectedDate)")
        }
       
    }

    
    //MARK:- server
    func  getAssignmentResponselist() {
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.ClassAssignment.rawValue + "\(classId)/")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)

        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
           // showActivityIndicator(decision: false, removeAllIndicators: self, animated: true)
            removeAllIndicators(inViewConroller: self)
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                    return
            }
            self.arrListing = result
            self.tblView.reloadData()
        }
        
    }

    func  getClassStudentsResponselist() {
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.ClassStudents.rawValue + "\(classId)/?limit=100&offset=0")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                    return
            }
            self.classStudentsList = result
            self.tblView.reloadData()
            
        }
        
    }

    func  getClassTeachersResponselist(){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.ClassTeachers.rawValue + "\(classId)/?limit=100&offset=0")
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            
            guard response != nil ,
                let result = response!["results"] as? [[String:AnyObject]] else {
                    PKSAlertController.alert(APP_NAME, message: "You do not have permission to perform this action.")
                    return
            }
            self.classTeachersList = result
            self.tblView.reloadData()
            
        }
        
    }
    
    func  inviteTeachersResponselist(params:[String:Any]){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.inviteTeacherToClass.rawValue)!
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        let header = ["Authorization" : "Token \(key)"]//;charset=UTF-8
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let key = response!["error"] as? Int else {
                    if response!["error"] as? Int != 1 {
                        self.addTeacherView.isHidden = true
                        self.clearAllTextFields()
                        showHudWithMessage(message: response!["message"] as? String ?? "Invitation Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
                        
                    } else {
                        showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
                    }
                    return
            }
            
            
            if key != 1 {
                self.addTeacherView.isHidden = true
                self.clearAllTextFields()
                showHudWithMessage(message: response!["message"] as? String ?? "Invitation Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
               
            } else {
                 showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
            }
            
        }
    }
    
    func  inviteStudentResponselist(params:[String:Any]) {
        
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.inviteNewStudentToClass.rawValue)!

        let key = AppHelper.userDefaultForKey(key: "Key")
        let header = ["Authorization" : "Token \(key)"]//;charset=UTF-8
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let key = response!["error"] as? Int else {
                    if response!["error"] as? Int != 1 {
                        self.addTeacherView.isHidden = true
                        self.clearAllTextFields()
                        showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
                        
                    } else {
                        showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
                    }
                    return
            }

            if key != 1 {
                self.addStudentView.isHidden = true
                self.clearAllTextFields()
                showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
                
            } else {
                showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
            }

        }
        
    }
    
    func addAssignment(params:[String:Any]){
        
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.addAssignment.rawValue)!
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        let header = ["Authorization" : "Token \(key)"]//;charset=UTF-8
        
        showActivityIndicator(decision: true, inViewConroller: self, animated: true)
        PCNetworkManager.sharedInstance.hitPostService(url: url, params: params,header: header) { (response) in
            showActivityIndicator(decision: false, inViewConroller: self, animated: true)
            guard response != nil ,
                let key = response!["error"] as? Int else {
                    if response!["error"] as? Int != 1 {
                        self.addAssignmentView.isHidden = true
                        self.clearAllTextFields()
                        self.getAssignmentResponselist()
                        showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
                        
                    } else {
                        showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
                    }
                    return
            }
            
            if key != 1 {
                self.addStudentView.isHidden = true
                self.clearAllTextFields()
                showHudWithMessage(message: response!["message"] as? String ?? "Sent!", inViewConroller: self, animated: true, hideAfter: 3.0)
                
            } else {
                showHudWithMessage(message: response!["message"] as? String ?? errorMessage.somethingWentWrng, inViewConroller: self, animated: true, hideAfter: 3.0)
            }
            
        }
        
    }
    
    //MARK:- actions
    
    @IBAction func btnBackClicked(sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAssignmentClicked(sender:UIButton) {
        tabSelected = "Assignment"
        getAssignmentResponselist()
    }
    
    @IBAction func btnPeopleClicked(sender:UIButton) {
        tabSelected = "People"
        getClassStudentsResponselist()
        getClassTeachersResponselist()
    }
    
    @objc func addUSerButtonTapped(sender:UIButton) {
        
        if(sender.tag == 101) {
            //teacher
            addStudentView.isHidden = true
            addAssignmentView.isHidden = true

            addTeacherView.isHidden = false
        } else {
            //student
            addStudentView.isHidden = false
            addAssignmentView.isHidden = true
            addTeacherView.isHidden = true
        }
    }
    
    @objc func addAssignmentButtonTapped(sender:UIButton) {
        addAssignmentView.isHidden = false
        addTeacherView.isHidden = true
        addStudentView.isHidden = true
    }
    
    func clearAllTextFields() {
        
        addAssignmentView.isHidden = true
        addTeacherView.isHidden = true
        addStudentView.isHidden = true
        
        emailTeacher.text = ""
        firstName.text = ""
        firstName.text = ""
        lastName.text = ""
        studentEmail.text = ""
        studentPassword.text = ""
        
        assModule.text = ""
        assName.text = ""
        assTopic.text = ""
        assDueDate.text = ""
    }
    
   
    @IBAction func inviteStudent(sender:UIButton) {
        self.view.endEditing(true)
        
        let param:[String: Any] = ["classID": "\(classId)",
            "email": "\(studentEmail.text?.trimmingCharacters(in: .whitespaces) ?? "")",
            "first_name": "\(firstName.text ?? "")",
            "id": "",
            "last_name": "\(lastName.text ?? "")",
            "password": "\(studentPassword.text ?? "")"]
        inviteStudentResponselist(params: param)
    }
    
    @IBAction func inviteTeacher(sender:UIButton) {
        self.view.endEditing(true)
        
        let param:[String: Any] = ["classID": "\(classId)",
            "email": "\(emailTeacher.text?.trimmingCharacters(in: .whitespaces) ?? "")",
            ]
        inviteTeachersResponselist(params: param)
    }
    
    @IBAction func addAssignment(sender:UIButton) {
        
        self.view.endEditing(true)
        let param:[String: Any] = ["class_id": "\(classId)",
            "module": "\(assModule.text ?? "")",
            "assignmentID": "",
            "name": "\(assName.text ?? "")",
            "topic": "\(assTopic.text ?? "")",
            "due_date": "\(assDueDate.text ?? "")"
        ]
        addAssignment(params: param)
    }
    
    @IBAction func cancelAssignment(sender:UIButton) {
        addAssignmentView.isHidden = true
    }
    
    @IBAction func closeAssignment(sender:UIButton) {
        addAssignmentView.isHidden = true
    }
    
    @IBAction func cancelInviteStudent(sender:UIButton) {
        addStudentView.isHidden = true
    }
    
    @IBAction func cancelInviteTeacher(sender:UIButton) {
        addTeacherView.isHidden = true
    }
    
    @IBAction func closeInviteStudent(sender:UIButton) {
        addStudentView.isHidden = true
    }
    
    @IBAction func closeInviteTeacher(sender:UIButton) {
        addTeacherView.isHidden = true
    }

}


extension AllClassesViewController : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tabSelected == "Assignment" ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tabSelected == "Assignment" {
            return arrListing.count > 0 ? arrListing.count : 0
        } else {
            if section == 0 {
                return classTeachersList.count
            } else {
                return classStudentsList.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tabSelected == "Assignment" {
            
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "ClassAssignmentTableViewCell", for: indexPath) as? ClassAssignmentTableViewCell else {
                return UITableViewCell()
            }
            cell.setData(dict: arrListing[indexPath.item])
            cell.selectionStyle = .none
            return cell
        } else {
            guard let cell  = tableView.dequeueReusableCell(withIdentifier: "ClassPeopleTableViewCell", for: indexPath) as? ClassPeopleTableViewCell else {
                return UITableViewCell()
            }
            if indexPath.section == 0 {
                cell.setUpData(data: classTeachersList[indexPath.item])
            } else {
                cell.setUpData(data: classStudentsList[indexPath.item])
            }
            
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tabSelected == "Assignment" {
            let vc = mainstoryBoard.instantiateViewController(withIdentifier: "CreatedAssignmentDetailViewController") as? CreatedAssignmentDetailViewController
            vc?.id = arrListing[indexPath.item]["pk"] as? Int ?? 0
            self.navigationController?.pushViewController(vc!, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
            
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "StudentTeacherHeader") as? StudentTeacherHeader else {
            return nil
        }
        
        header.viewSeprator.isHidden = true
        if tabSelected == "Assignment" {
            
            header.lblName.text = "Add Assignment"
            header.addUserButton.tag = 102
            
            header.addUserButton.addTarget(self, action: #selector(addAssignmentButtonTapped(sender:)),
                                           for: .touchUpInside)
            return header
        } else {
            if section == 0 {
                header.lblName.text = "Teachers"
                header.addUserButton.tag = 101
            } else {
                header.lblName.text = "Students"
                header.addUserButton.tag = 102
            }
            header.addUserButton.addTarget(self, action: #selector(addUSerButtonTapped(sender:)),
                                           for: .touchUpInside)
            return header
        }
        
        
        //return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if tabSelected != "Assignment" {
//            return 50
//        }
        return 70
    }
    
    
}
