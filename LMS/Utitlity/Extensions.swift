//
//  Extensions.swift
//  JetMate
//
//  Created by cheena
//  Copyright © 2019 Appincuba. All rights reserved.
//

import Foundation
import UIKit


extension UIButton {
    
//    @objc func cornerRadius(radius:CGFloat) {
//        layer.cornerRadius = radius
//        contentMode = UIView.ContentMode.scaleAspectFill
//        clipsToBounds = true
//    }
    
    func addBottomShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
}


extension UITextField {
    
    //Provide bottom border in textfield
    func bottomBorder(borderColor color:UIColor, borderWidth width:CGFloat) {
        let bottomBorder:CALayer = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        bottomBorder.backgroundColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.4).cgColor
        self.layer.addSublayer(bottomBorder)
    }
    
    //In case you want to use icon in the textfield
    func addImage(image:UIImage) {
        let paddingView:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: image.size.width + 22, height: image.size.height))
        let imageView:UIImageView = UIImageView(frame: CGRect(x: 11, y: 0, width: image.size.width, height: image.size.height))
        imageView.image = image;
        paddingView.addSubview(imageView)
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
}

extension String {
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var isValidPassword: Bool {
        let passwordRegex = "^(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,12}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        return passwordTest.evaluate(with: self)
    }
    
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}

extension UIView {
    func addShadowTo() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.1
        layer.shadowRadius = 3.0
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
}
extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UILabel {
    func setBottomBorder() {
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 235/255.0, green: 235/255.0, blue: 235/255.0, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}


extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension UIView {
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient11(colours: colours, locations: nil)
    }
    
    func applyGradient11(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func cornerRadius(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.contentMode        = .scaleAspectFill
        self.clipsToBounds      = true
    }
    
    func roundBorder() {
        self.cornerRadius(radius: self.bounds.size.height / 2)
    }
    
    func drop(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
        // self.layer.shadowOffset         =   CGSize(width: 6.0, height:0.0)
        
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   = UIScreen.main.scale
        self.layer.zPosition = 1
        
    }
    func dropViewData(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
        // self.layer.shadowOffset         =   CGSize(width: 6.0, height:0.0)
        
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   = UIScreen.main.scale
        //self.layer.zPosition = 1
        
    }
    func dropView(shadow color: UIColor, shadowOpacity: Float, offset: CGSize, radius: CGFloat) {
        
        self.layer.shadowColor          =   color.cgColor
        self.layer.shadowOpacity        =   shadowOpacity
        self.layer.shadowOffset         =   CGSize(width: -1.0, height:3.0)
        
        self.layer.shadowRadius         =   radius
        self.layer.shouldRasterize      =   true
        self.layer.rasterizationScale   =   UIScreen.main.scale
        self.layer.zPosition            =   1
        
    }
    
    @IBInspectable var addShadow :Bool {
        get {
            
            return true
            
        }
        set {
            if addShadow {
                self.dropView(shadow: HeaderViewShadow.color, shadowOpacity: HeaderViewShadow.opacity, offset: HeaderViewShadow.offset, radius: HeaderViewShadow.radius)
                
                self.layer.zPosition = 1
            }
        }
    }
    
}

@IBDesignable extension UIButton {
    @IBInspectable var cornerRadius :CGFloat {
        
        get {
            return layer.cornerRadius
        }
        
        set {
            
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var cornerRadiusOneSide :CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            let path = UIBezierPath(roundedRect:self.bounds, byRoundingCorners:[.topRight,.bottomRight], cornerRadii: CGSize(width: cornerRadiusOneSide, height: cornerRadiusOneSide))
            let maskLayer = CAShapeLayer()
            
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
            layer.masksToBounds = true
            
        }
    }
    
}

@IBDesignable extension UIView{
    
    @IBInspectable var cornerRadiusOfView :CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }

    
}


@IBDesignable extension UIImageView{
    @IBInspectable var cornerRadius :CGFloat{
        get{
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    
}

public extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
}

