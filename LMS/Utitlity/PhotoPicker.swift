//
//  PhotoPicker.swift
//  LMS
//
//  Created by Wemonde on 04/08/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PhotoPicker: NSObject {
    
    var responseHandler : ((UIImage?) -> Void)!
    var presentController : UIViewController!
    var imagePicker = UIImagePickerController()

    internal static let sharedInstance: PhotoPicker = {
        return PhotoPicker()
    }()

    
    func initialize(_ presentVC:UIViewController){
        presentController = presentVC
        imagePicker.delegate = self
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        presentVC.present(alert, animated: true, completion: nil)
    }
    
    
    func openCamera(){
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch (authStatus){
        case .notDetermined,.restricted,.denied:
            showAlert(title: "Unable to access the Camera", message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.")
        case .authorized:
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                imagePicker.sourceType = UIImagePickerController.SourceType.camera
                imagePicker.allowsEditing = true
                presentController.present(imagePicker, animated: true, completion: nil)
            }
            else{
                let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                presentController.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        presentController.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default, handler: { _ in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        })
        alert.addAction(settingsAction)
        presentController.present(alert, animated: true, completion: nil)
    }


    

}

extension PhotoPicker : UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            print("pickedImage==== \(pickedImage.description)")
            responseHandler(pickedImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
}
