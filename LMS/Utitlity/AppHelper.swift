//
//  AppHelper.swift
//  JetMete
//
//  Created by shared
//  Copyright © 2017 Affle Appstudioz. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD


var activityIndicator:UIActivityIndicatorView?

//This class contains common Methods used in app.


class AppHelper: NSObject, UINavigationControllerDelegate, UIAlertViewDelegate {
    
    //MARK: - Getting Storyboard
    
    static func executeOnMainThread(_ executionBlock: @escaping () -> Void) {
        OperationQueue.main.addOperation({
            executionBlock()
        })
    }
    
    class func Storyboard() -> UIStoryboard {
        
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    //  MARK:- NSUser Defaults
    
    //MARK: User default
    
    // save to user default
    class func saveToUserDefault (value: AnyObject? , key: String!) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key! as String)
        defaults.synchronize()
    }
    
    // Get from user default
    
    class func userDefaultForKey ( key: String?) -> String{
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key! as String)
        if (value != nil) {
            return value!
        }
        return ""
    }
    
    // Remove from user default
    class  func removeFromUserDefaultForKey(key: NSString!) {
        let defaults = UserDefaults.standard
        let value = defaults.string(forKey: key! as String)
        if (value != nil) {
            defaults.removeObject(forKey: key! as String)
        }
        defaults.synchronize()
        
    }
    
    class func dictUserDefaultsForKey(key: String?)->AnyObject? {
        let defaults = UserDefaults.standard
        if defaults.value(forKey: key!) != nil{
            let data = defaults.value(forKey: key!) as AnyObject
            return data
        }
        return nil
    }
    
    
    
    class func setDictToUserDefault(dict: Any,key:String) {
        let data = NSKeyedArchiver.archivedData(withRootObject: dict)
        let userDefaults = UserDefaults.standard
        userDefaults.set(data, forKey:key)
    }
    
    class func getDictFromUserDefault(key:String) -> AnyObject? {
        if let data = UserDefaults.standard.object(forKey: key) as? NSData{
            let object = NSKeyedUnarchiver.unarchiveObject(with: data as Data)
            return object as AnyObject;
        }
        return nil
    }


    
   class func EmptyMessage(message:String, tableView:UITableView) {
        let rect = CGRect(origin: CGPoint(x: 20,y :0), size:CGSize.init(width: screenWidth-80, height: 80))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.lightGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = .none;
    }
    
    
    
    //MARK: AlertView
    class func showALertWithTag(title:String, message:String?,delegate:AnyObject!, cancelButtonTitle:String?, otherButtonTitle:String?)
    {
        
        
        var alertController = UIAlertController()
        
        alertController = UIAlertController(title: title, message: message , preferredStyle: UIAlertController.Style.alert)
        
        
        
        let ok:UIAlertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action) -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(ok)
        
        //        let topViewController = APPDELEGATE.window?.rootViewController
        //        topViewController?.present(alertController, animated: true, completion: nil)
    }
}

// MARK:- Global Methods

//Toast Message
func showHudWithMessage(message:String, inViewConroller vc:UIViewController, animated:Bool, hideAfter seconds:TimeInterval) {
    
    let hud = MBProgressHUD.showAdded(to: vc.view, animated: true)
    hud.offset.y = 450
    hud.bezelView.color = UIColor(red: 0/255.0, green: 151/255.0, blue: 180/255.0, alpha: 1)
    hud.contentColor = UIColor.white
    hud.mode = MBProgressHUDMode.text
    hud.detailsLabel.text = message
    hud.show(animated: true)
    hud.hide(animated: true, afterDelay: seconds)
}

//MARK:- Activity Indicator

func showActivityIndicator(decision:Bool, inViewConroller vc:UIViewController, animated:Bool) {
    if(decision) {
        MBProgressHUD.showAdded(to: vc.view, animated: animated)
    }
    else {
        MBProgressHUD.hide(for: vc.view, animated: animated)
    }
}

func removeAllIndicators(inViewConroller vc:UIViewController) {
    
    MBProgressHUD.hideAllHUDs(for: vc.view, animated: true)
}

