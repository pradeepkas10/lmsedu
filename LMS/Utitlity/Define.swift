//
//  Define.swift
//  JetMate
//
//  Created by cheena
//  Copyright © 2019 Appincuba. All rights reserved.
//

import Foundation
import UIKit


let APP_NAME = "LMS"
var APP_FOR = "Student"
var IS_Google_LoggedIn = false
let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height

let mainstoryBoard = UIStoryboard(name: "Main", bundle: nil)

let appDelegate = UIApplication.shared.delegate as! AppDelegate




//MARK: - Global Print Method
func printLogs <T> (value:T)
{
    print(value)
}

//MARK: - Enumerations
enum ServicesURLs  :String {
    case BaseUrl            = "https://api.progressay.mynividata.in/"
}


//MARK: - Service Methods

enum ServiceMethods : String{
    
    case registration                 = "rest-auth/registration/"
    case registrationEmailVerify      = "rest-auth/registration/verify-email/"
    case login                        = "rest-auth/login/"
    case loginGoogle                  = "rest-auth/google/"
    case loginFacebook                = "rest-auth/facebook/"
    case profile                      = "rest-auth/user/"
    
    case TeacherList                  = "market-place/marker-listing/"
    case NotificationList              = "common/all-notification/"
    case ChangePassword                = "rest-auth/password/change/"
    case forgotPassword                = "rest-auth/password/reset/"
    case MyJobs                        = "market-place/my-jobs/"
    case AssignmentDetail              = "market-place/create-job/"
    case InitialData                   = "common/initial-data/"
    case GetClasses                    = "class/get_student_classes/"
    case GetAllClassTeacher            = "class/teacher/all-classes"
    case ClassList                     = "class/get-class/"
    case ClassAssignment               = "class/get-class-assignments/"
    case ClassStudents                 = "class/get-class-students/"
    case ClassTeachers                 = "class/get-class-teachers/"
    
    case ApplicationList               = "market-place/my-job-application-listing/"
    case teacherDetails                = "users/user-public-profile/"
    case jobStatus                     = "market-place/check-job-application/"
    case selectMarker                  = "market-place/select-marker/"
    
    case userDropDown                  = "users/user-dropdown-list/"
    case cityDropDown                  = "users/city-dropdown-list/"
    case userRole                      = "users/user-role/"
    
    case inviteNewStudentToClass       = "users/create-update"
    case inviteTeacherToClass          = "class/teacher/invite-teacher/"
    case addAssignment                 = "class/assignment/"
    
    
    case createPersonalDetail          = "market-place/payment/create-account/"
    case getPersonalDetail             = "market-place/payment/user-account-details/"
    case getUserBankDetail             = "market-place/payment/user-bank-account-details/"
}


struct HeaderViewShadow {
    static let color: UIColor   = UIColor.lightGray
    static let radius: CGFloat  = 8
    static let offset: CGSize   =  CGSize.init(width: 10, height: 10)//CGSize.zero
    static let opacity: Float   = 0.5
}




struct errorMessage {
    static let somethingWentWrng = "Something went wrong. Please try after some time."
    static let credentialsProblem = "Unable to log in with provided credentials."
    
}

















