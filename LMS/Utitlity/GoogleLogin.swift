//
//  GoogleLogin.swift
//  LMS
//
//  Created by Wemonde on 27/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import GoogleSignIn

class GoogleLogin: NSObject,GIDSignInDelegate,GIDSignInUIDelegate {
    
    
    var responseHandler : ((AnyObject?) -> Void)!
    var presentController : UIViewController!
    
    internal static let sharedInstance: GoogleLogin = {
        return GoogleLogin()
    }()

    
    func initializeLogin(presentVC:UIViewController){
        GIDSignIn.sharedInstance().clientID = "827947645057-tqfdjmj9e1arch8o49apepdevnjvqflg.apps.googleusercontent.com"
        
        
        
        //"13779374617-h5dpvlt8irvu9ij73sp3uj9c55bvokv9.apps.googleusercontent.com"
        
//"827947645057-gostsli4div7qlgu95t6ms75405tknf4.apps.googleusercontent.com"
        
            //"753390445841-glnturn5bqibrad1qso3q0h2ve5h8cju.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.uiDelegate = self
        presentController = presentVC
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
            print("\(user.profile.email)")
            print("\(user.authentication.accessToken)")
            responseHandler(user.authentication.accessToken as? AnyObject)
        }
    }

    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        print("\(error.debugDescription)")

        responseHandler(nil)
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        presentController.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
       presentController.dismiss(animated: true, completion: nil)
    }
    
    func signout() {
        GIDSignIn.sharedInstance().signOut()
    }
    

}
