//
//  FacebookLogin.swift
//  LMS
//
//  Created by Wemonde on 27/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit


class FacebookLogin: NSObject {
    
    var responseHandler : ((Any?) -> Void)!
    
    internal static let sharedInstance: FacebookLogin = {
        return FacebookLogin()
    }()

    
    func intializeFBLogin(controller:UIViewController){
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile" , "email"], from: controller) { (result, error) in
            if (error == nil){
                if (result?.isCancelled)!{
                    self.responseHandler(nil)
                    return
                }
                self.getFBUserData()
                
            }
        }
    }
        
    func getFBUserData(){
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    print(result!)
                    self.responseHandler(AccessToken.current!.tokenString)
                }else{
                    self.responseHandler(nil)
                }
            })
        }
    }
    
}
