//
//  ReconizeHandWrittenText.swift
//  LMS
//
//  Created by Wemonde on 22/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import UIKit

class ReconizeHandWrittenText: NSObject {
    
    let url = "https://westeurope.api.cognitive.microsoft.com/vision/v1.0/recognizeText"
    
    //"https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/recognizeText"
    
    
//    params === ["username": "AZURE_COGNITIVE_SERVICE_URL=\'https://westeurope.api.cognitive.microsoft.com/vision/v2.0/RecognizeText\' AZURE_COGNITIVE_SERVICE_SUBSCRIPTION_KEY=\'fee1d950cc324c978d1f9369105b3d95\'", "password": "Qwerty@123"]

    
    /// Your private API key. If you havn't changed it yet, go ahead!
    let key = CognitiveServicesApiKeys.ComputerVision.rawValue
    
    
    enum RecognizeCharactersErrors: Error {
        case unknownError
        case imageUrlWrongFormatted
        case emptyDictionary
    }
    
    func recognizeCharactersWithRequestObject(_ requestObject: Any, completion: @escaping (_ response: [String:AnyObject]? , HTTPURLResponse? ) -> Void) throws {
        // Generate the url
        let requestUrlString = url + "?entities=true&handwriting=true"
        print("url === \(requestUrlString)")
        let requestUrl = URL(string: requestUrlString)
        
        var request = URLRequest(url: requestUrl!)
        request.setValue(key, forHTTPHeaderField: "Ocp-Apim-Subscription-Key")
        
        // Request Parameter
        if let path = requestObject as? String {
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = "{\"url\":\"\(path)\"}".data(using: String.Encoding.utf8)
            print("url == \(path))")
            
        }
        else if let imageData = requestObject as? Data {
            request.setValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
            request.httpBody = imageData
            print("imageData == \(imageData.count))")
        }
        
        request.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            if let response11 =  response as? HTTPURLResponse{
                if response11.statusCode == 202{
                    completion(nil,response11)
                    return
                }
            }
            
            if error != nil{
                print("Error -> \(error!)")
                completion(nil,nil)
                return
            }else{
                let results = try! JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                DispatchQueue.main.async {
                    completion(results,nil)
                }
            }
            
        }
        task.resume()
        
    }
    
    
    
    func getTextFromHeaderURL(_ strURL:String , completion: @escaping (_ response: [String:AnyObject]? ) -> Void) throws {
        let requestUrl = URL(string: strURL)
        print("requestUrl == \(requestUrl!)")
        var request = URLRequest(url: requestUrl!)
        request.setValue(key, forHTTPHeaderField: "Ocp-Apim-Subscription-Key")
        let task = URLSession.shared.dataTask(with: request){ data, response, error in
            
            if error != nil{
                print("Error -> \(error!)")
                 completion(nil)
                return
            }else{
                let results = try! JSONSerialization.jsonObject(with: data!, options: []) as? [String:AnyObject]
                DispatchQueue.main.async {
                    completion(results)
                }
            }
            
        }
        task.resume()
        
    }
    
    
    func extractLine(dict:[String:AnyObject])->String{
        var finalString = ""
        guard let sucess =  dict["status"] as? String ,
            sucess == "Succeeded" else { return finalString }
        
        if dict["recognitionResult"] != nil {
            if let dictLine = dict["recognitionResult"] as? [String:AnyObject]{
                if let arrLines = dictLine["lines"] as? [AnyObject]{
                    for ele in arrLines{
                        if let dictText = ele as? [String:AnyObject]{
                            print(dictText["text"] as? String ?? "no data")
                            finalString = finalString + (dictText["text"] as? String ?? "") + "\n"
                        }
                    }
                }
            }
        }
        return finalString
        
    }
    
    
}
