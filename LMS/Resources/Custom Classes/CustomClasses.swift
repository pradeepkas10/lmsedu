//
//  CustomClasses.swift
//  CallApp
//
//  Created by Wemonde on 02/06/19.
//  Copyright © 2019 Demo. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField:UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("borderForDiffColor")
        self.borderForDiffColor()
    }
    
    
    func borderForDiffColor(){

        self.leftView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftViewMode = .always
        
        self.rightView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: self.frame.height))
        self.rightViewMode = .always
        
//        if let layers = self.layer.sublayers {
//            for layer: CALayer in layers {
//                if layer.name == "bottomLine" || layer.name == "leftline" || layer.name == "right"  ||  layer.name == "top"{
//                    layer.removeFromSuperlayer()
//                }
//            }
//        }
//        
//        let topRight = UIColor.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1/0)
//        let bottomLeft = UIColor.init(red: 201/255, green: 202/255, blue: 203/255, alpha: 1/0)
//        
//        let height:CGFloat = 1.5
//        
//        let bottomLine = CALayer()
//        bottomLine.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
//        bottomLine.backgroundColor = bottomLeft.cgColor
//        bottomLine.name = "bottomLine"
//        self.layer.addSublayer(bottomLine)
//        
//        let leftline = CALayer()
//        leftline.frame = CGRect(x: 0, y: 0, width: height, height: self.frame.height)
//        leftline.backgroundColor = bottomLeft.cgColor
//        leftline.name = "leftline"
//        self.layer.addSublayer(leftline)
//        
//        let right = CALayer()
//        right.frame = CGRect(x: self.frame.width-height, y: 0, width: height, height: self.frame.height)
//        right.backgroundColor = topRight.cgColor
//        right.name = "right"
//        self.layer.addSublayer(right)
//        
//        let top = CALayer()
//        top.frame = CGRect(x: 0, y: 0, width: self.frame.width, height:height)
//        top.backgroundColor = topRight.cgColor
//        top.name = "top"
//        
//        self.layer.addSublayer(top)
        
        
    }
    
}

class CustomTextView:UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("borderForDiffColor")
        self.borderForDiffColor()
    }
    
    
    func borderForDiffColor(){
        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottomLine" || layer.name == "leftline" || layer.name == "right"  ||  layer.name == "top"{
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        let topRight = UIColor.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1/0)
        let bottomLeft = UIColor.init(red: 201/255, green: 202/255, blue: 203/255, alpha: 1/0)
        
        let height:CGFloat = 1.5
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
        bottomLine.backgroundColor = bottomLeft.cgColor
        bottomLine.name = "bottomLine"
        self.layer.addSublayer(bottomLine)
        
        let leftline = CALayer()
        leftline.frame = CGRect(x: 0, y: 0, width: height, height: self.frame.height)
        leftline.backgroundColor = bottomLeft.cgColor
        leftline.name = "leftline"
        self.layer.addSublayer(leftline)
        
        let right = CALayer()
        right.frame = CGRect(x: self.frame.width-height, y: 0, width: height, height: self.frame.height)
        right.backgroundColor = topRight.cgColor
        right.name = "right"
        self.layer.addSublayer(right)
        
        let top = CALayer()
        top.frame = CGRect(x: 0, y: 0, width: self.frame.width, height:height)
        top.backgroundColor = topRight.cgColor
        top.name = "top"
        
        self.layer.addSublayer(top)
        
        
    }
    
}


class CustomView:UIView {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        print("borderForDiffColor")
        self.borderForDiffColor()
    }
    
    
    func borderForDiffColor(){
        
        if let layers = self.layer.sublayers {
            for layer: CALayer in layers {
                if layer.name == "bottomLine" || layer.name == "leftline" || layer.name == "right"  ||  layer.name == "top"{
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        let topRight = UIColor.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1/0)
        let bottomLeft = UIColor.init(red: 201/255, green: 202/255, blue: 203/255, alpha: 1/0)
        
        let height:CGFloat = 1.5
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - height, width: self.frame.width, height: height)
        bottomLine.backgroundColor = bottomLeft.cgColor
        bottomLine.name = "bottomLine"
        self.layer.addSublayer(bottomLine)
        
        let leftline = CALayer()
        leftline.frame = CGRect(x: 0, y: 0, width: height, height: self.frame.height)
        leftline.backgroundColor = bottomLeft.cgColor
        leftline.name = "leftline"
        self.layer.addSublayer(leftline)
        
        let right = CALayer()
        right.frame = CGRect(x: self.frame.width-height, y: 0, width: height, height: self.frame.height)
        right.backgroundColor = topRight.cgColor
        right.name = "right"
        self.layer.addSublayer(right)
        
        let top = CALayer()
        top.frame = CGRect(x: 0, y: 0, width: self.frame.width, height:height)
        top.backgroundColor = topRight.cgColor
        top.name = "top"
        
        self.layer.addSublayer(top)
        
        
    }
    
}
