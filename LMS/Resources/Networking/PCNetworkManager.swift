//
//  PCNetworkManager.swift
//  VLCC
//
//  Created by Wemonde on 22/03/18.
//  Copyright © 2018 Wemonde. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import MobileCoreServices

public class PCNetworkManager {
    
    //MARK:- Shared Instance
    /**
     A shared instance of `Manager`, used by top-level Alamofire request methods, and suitable for use directly
     for any ad hoc requests.
     */
    internal static let sharedInstance: PCNetworkManager = {
        return PCNetworkManager()
    }()
    
    
    internal func checkInternetConnection() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    
    //MARK: ************* HIT GET SERVICE *************
    func hitGetService(strURL:String,header:[String:String] ,handler:@escaping ((AnyObject?)-> Void)) {
        print("url == \(strURL)")
        
        let encodeURl = strURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if NetworkReachabilityManager()?.isReachable == true {
            request(encodeURl!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                if response.result.isSuccess{
                    print(response.result.value)
                    handler(response.result.value as AnyObject?)
                }
                else{
                    handler(nil)
                }
            }  }else{
            handler(nil)
            //  SVProgressHUD.dismiss()
            PKSAlertController.alert(APP_NAME, message:"No internet available" , acceptMessage: "Ok", acceptBlock: {
            })
            
        }
        
        
    }
    
    //MARK:- Public Method post
    
    internal func hitPostService(url: URL ,params:[String:Any],header:[String:String], handler:@escaping (([String:AnyObject]?) -> Void)) {
        if NetworkReachabilityManager()?.isReachable == true {
            print("url == \(url)")
            print("params === \(params)")
            request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                print(response)
                if response.result.isSuccess{
                    print(response.result.value)
                    handler(response.result.value as? [String:AnyObject])
                }
                else{
                    handler(nil)
                    //PKSAlertController.alert("ERROR", message: "ERROR")
                }
            }
        }else
        {
            handler(nil)
            PKSAlertController.alert(APP_NAME, message: "no internet Connection")
            
        }
    }
    
    internal func hitPatchService(url: URL ,params:[String:Any],header:[String:String], handler:@escaping (([String:AnyObject]?) -> Void)) {
        if NetworkReachabilityManager()?.isReachable == true {
            print("url == \(url)")
            print("params === \(params)")
            request(url, method: .patch , parameters: params, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                print(response)
                if response.result.isSuccess{
                    print(response.result.value)
                    handler(response.result.value as? [String:AnyObject])
                }
                else{
                    handler(nil)
                    //PKSAlertController.alert("ERROR", message: "ERROR")
                }
            }
        }else
        {
            handler(nil)
            PKSAlertController.alert(APP_NAME, message: "no internet Connection")
            
        }
    }

    
    
    func hitPostWithNativeMethod(url: URL ,params:[String:Any],header:[String:String], handler:@escaping (([String:AnyObject]?) -> Void)){
        
        print(url)
        print(params)
        print(header)
        
        let session = URLSession.shared
        var request = URLRequest.init(url: url)
        request.allHTTPHeaderFields = header
        request.httpMethod = "POST"
        
        request.httpBody = params.percentEscaped().data(using: .utf8)
        
        session.dataTask(with: request) { (data, response, error) in
            print(response) //,
            DispatchQueue.main.async {
                guard let response = response as? HTTPURLResponse ,response.statusCode == 200 ,let data = data else {
                    handler(nil)
                    return
                }
                do {
                    let result =  try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions.mutableContainers)
                    print(result)
                    handler(result as? [String:AnyObject])
                    
                }catch let error{
                    print("error == \(error)")
                    handler(nil)
                    
                }
            }
            
            }.resume()
        
        
    }
    
    
    
    
    
    internal func hitDeleteService(url: URL ,params:[String:Any],header:[String:String], handler:@escaping (([String:AnyObject]?) -> Void)) {
        if NetworkReachabilityManager()?.isReachable == true {
            print("url == \(url)")
            print("params === \(params)")
            request(url, method: .delete , parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
                print(response)
                if response.result.isSuccess{
                    print(response.result.value)
                    handler(response.result.value as? [String:AnyObject])
                }
                else{
                    handler(nil)
                    //PKSAlertController.alert("ERROR", message: "ERROR")
                }
            }
        }else
        {
            handler(nil)
            PKSAlertController.alert(APP_NAME, message: "no internet Connection")
            
        }
    }
    
    func  updateProfileInUserDefault(handler:@escaping ((Bool) -> Void)){
        let url =  URL.init(string: ServicesURLs.BaseUrl.rawValue + ServiceMethods.profile.rawValue)
        var header = [String:String]()
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        header = ["Authorization" : "Token \(key)"]
        
        PCNetworkManager.sharedInstance.hitGetService(strURL: (url?.absoluteString)!, header: header) { (response) in
            guard response != nil ,
                let result = response as? [String:AnyObject] else {
                    handler(false)
                    return
            }
            AppHelper.setDictToUserDefault(dict: result, key: "UserData")
            handler(true)
        }
        
    }

    
    
    
    
    private func getPrintableParamsFromJson(postData: Dictionary<String, AnyObject>) -> String {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: postData, options:JSONSerialization.WritingOptions.prettyPrinted)
            let theJSONText = String(data:jsonData, encoding:String.Encoding.ascii)
            return theJSONText ?? ""
        } catch let error as NSError {
            print("errror === \(error.description)")
            return ""
        }
    }
    
    func requestWith(endUrl: String, files: [Data]?, parameters: [String : Any], onCompletion: ((AnyObject?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        
        let url = URL.init(string: endUrl)
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        
        let headers: HTTPHeaders = [
            "Authorization": "Token \(key)",
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            var index = 0
            
            for file in files!{
                multipartFormData.append(file, withName: "files", fileName: "test\(index).txt", mimeType: "text/plain")
                //                multipartFormData.append(file, withName: "rubric_files", fileName: "test\(index).txt", mimeType: "text/plain")
                index = index + 1
            }
            
            
            
        }, usingThreshold: UInt64.init(), to: url!, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("uploaded \(response)")
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    onCompletion?((response.result.value as AnyObject))
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
    
    func uploadImage(endUrl: String, files: Data, parameters: [String : Any],withName:String, onCompletion: @escaping (AnyObject?) -> Void){
        
        let url = URL.init(string: endUrl)
        
        let key = AppHelper.userDefaultForKey(key: "Key")
        
        let headers: HTTPHeaders = [
            "Authorization": "Token \(key)",
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let mime = self.mimeType(for: files)
            print(mime)
            multipartFormData.append(files, withName: "avatar", fileName: "image.jpg", mimeType: mime)
            
            
        }, usingThreshold: UInt64.init(), to: url!, method: .patch, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("uploaded \(response)")
                    if let _ = response.error{
                        onCompletion(nil)
                        return
                    }
                    onCompletion((response.result.value as AnyObject))
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onCompletion(nil)
            }
        }
    }

    func mimeType(for data: Data) -> String {
        
        var b: UInt8 = 0
        data.copyBytes(to: &b, count: 1)
        
        switch b {
        case 0xFF:
            return "image/jpeg"
        case 0x89:
            return "image/png"
        case 0x47:
            return "image/gif"
        case 0x4D, 0x49:
            return "image/tiff"
        case 0x25:
            return "application/pdf"
        case 0xD0:
            return "application/vnd"
        case 0x46:
            return "text/plain"
        default:
            return "application/octet-stream"
        }
    }

    
    
    func updateData(){
        let todosEndpoint: String = "https://api.progressay.mynividata.in/rest-auth/user/"
//        let newTodo: [String: Any] = ["first_name": "My First Post", "last_name": "kumar", "city": 1270642,"country" :1269750, "bio" : "bio","rate" : "26.0" ,"skills" : "sdfsdfsdf" , "title" : "checing"];
        
        let newTodo =  ["first_name": "My First Post post ", "last_name": "kumar", "city": "1270642", "country": "1269750",
                        "bio": "bio dsff df afsd fds d fsf s",
                        "qualifications": [["qualification_id": "1"]],
                        "rate": "26.00",
                        "skills": "skillsskillsskills skills",
                        "subjects": [["subject_id": "1"], ["subject_id": "2"]],
            "title": "checing"] as [String : Any]

        let key = AppHelper.userDefaultForKey(key: "Key")
        
        let headers: HTTPHeaders = [
            "Authorization": "Token \(key)",
        ]
        
        
        request(todosEndpoint, method: .patch, parameters: newTodo, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                print("error calling POST on /todos/1")
                print(response.result.error!)
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any] else {
                print("didn't get todo object as JSON from API")
                print("Error: \(response.result.error)")
                return
            }
            
            print("json dta====  \(json)")
            
            
        }
        
    }
    
    
    func toUploadfiles(){
        
        let url  = URL.init(string: "https://api.progressay.mynividata.in/market-place/create-job/")
        
        var request = URLRequest.init(url: url!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 2.0)
        request.httpMethod = "POST"
        
        // Set Content-Type in HTTP header.
        let boundaryConstant = "Boundary-7MA4YWxkTLLu0UIW"; // This should be auto-generated.
        let contentType = "multipart/form-data; boundary=" + boundaryConstant
        
        
        let fileName = "test12.txt"
        let mimeType = "text/plain"
        let fieldName = "files"
        
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        
        
        
        
        
        
        
    }
    
    
    private func createBody(with parameters: [String: String]?, filePathKey: String, paths: [String], boundary: String) throws -> Data {
        var body = Data()
        
        if parameters != nil { 
            for (key, value) in parameters! {
                body.append("--\(boundary)\r\n")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.append("\(value)\r\n")
            }
        }
        
        for path in paths {
            let url = URL(fileURLWithPath: path)
            let filename = url.lastPathComponent
            let data = try Data(contentsOf: url)
            let mimetype = mimeType(for: path)
            
            body.append("--\(boundary)\r\n")
            body.append("Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(filename)\"\r\n")
            body.append("Content-Type: \(mimetype)\r\n\r\n")
            body.append(data)
            body.append("\r\n")
        }
        
        body.append("--\(boundary)--\r\n")
        return body
    }
    
    
    
    private func mimeType(for path: String) -> String {
        let url = URL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    
    
}


extension Data {
    mutating func append(_ string: String) {
        let data = string.data(
            using: String.Encoding.utf8,
            allowLossyConversion: true)
        append(data!)
    }
}


extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}

extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
