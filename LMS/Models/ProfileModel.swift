//
//  ProfileModel.swift
//  LMS
//
//  Created by Wemonde on 29/06/19.
//  Copyright © 2019 Ishika Gupta. All rights reserved.
//

import Foundation

struct Profile:Codable {
    
    let email: String
    let first_name: String
    let last_name: String
    let user_type: String
    let avatar: String?
    
    func fullName()->String{
        return first_name + " " + last_name
    }
    
    
}
